-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 18, 2018 at 10:36 PM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `RoutePlanning`
--

-- --------------------------------------------------------

--
-- Table structure for table `ACTUALITIES`
--

CREATE TABLE `ACTUALITIES` (
  `ID` int(11) NOT NULL,
  `HEADLINE` text NOT NULL,
  `CONTENT` text NOT NULL,
  `CREATED_AT` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ACTUALITIES`
--

INSERT INTO `ACTUALITIES` (`ID`, `HEADLINE`, `CONTENT`, `CREATED_AT`) VALUES
(1, 'Jujubes cotton candy gummies cake jujubes chupa', 'Cake jujubes wafer pie caramels gummies. Tiramisu lollipop dessert sweet roll biscuit candy canes macaroon cake. Chocolate cake cookie tart muffin chocolate cake danish sweet roll toffee chocolate. Donut macaroon soufflé. Sweet apple pie lemon drops lollipop topping candy canes wafer chupa chups. Chupa chups sweet roll danish biscuit jelly-o. Pastry chocolate cake oat cake oat cake brownie. Brownie topping chupa chups caramels powder gingerbread cake jujubes. Brownie powder muffin gingerbread cheesecake carrot cake toffee chocolate gingerbread. Wafer topping icing lollipop. Macaroon cheesecake cupcake liquorice jelly beans candy canes. Gingerbread tiramisu brownie marshmallow. Gummi bears oat cake cupcake apple pie.', '2018-05-13 20:14:30'),
(2, 'Macaroon jelly beans sugar plum croissant', 'Fruitcake biscuit bear claw marzipan gummies. Oat cake tiramisu gingerbread. Tart tart lollipop chocolate cake pie bonbon chocolate cake. Marzipan sugar plum dragée icing cupcake biscuit caramels tiramisu croissant. Bear claw caramels donut brownie cookie icing. Bear claw powder cookie dragée liquorice. Marzipan cake halvah apple pie powder cupcake sweet lemon drops muffin. Topping gummi bears pudding. Icing jelly beans bonbon. Toffee apple pie brownie chocolate cake wafer brownie. Chocolate bar jelly liquorice icing sesame snaps chocolate bar. Candy tart lemon drops wafer. Carrot cake dragée jelly beans. Brownie wafer marzipan pie fruitcake dessert.', '2018-05-13 20:15:20'),
(4, 'Nova aktualita', 'Toto je priklad pridania novej aktuality administratorom. Lorem Ipsum Dolor Sit Amet.\r\n', '2018-05-17 19:15:50'),
(5, 'Ako dlho trva pridanie', 'Novej aktuality, ??? Nullam tincidunt, magna iaculis euismod scelerisque, elit risus aliquam odio, ut finibus nulla est ut leo. Etiam nec orci eget neque euismod porttitor vitae et metus. Duis rhoncus purus quis odio cursus, sit amet efficitur mi tristique. Curabitur at augue vitae libero imperdiet venenatis. Morbi ut scelerisque est. Donec venenatis commodo egestas. Integer at dolor pulvinar, tempor justo at, tempus quam. ', '2018-05-17 19:44:30');

-- --------------------------------------------------------

--
-- Table structure for table `PEOPLE`
--

CREATE TABLE `PEOPLE` (
  `ID` int(2) DEFAULT NULL,
  `PRIEZVISKO` varchar(11) DEFAULT NULL,
  `MENO` varchar(10) DEFAULT NULL,
  `EMAIL` varchar(17) DEFAULT NULL,
  `SKOLA` varchar(70) DEFAULT NULL,
  `SKOLA_ADR` varchar(51) DEFAULT NULL,
  `ULICA` varchar(21) DEFAULT NULL,
  `PSC` int(5) DEFAULT NULL,
  `OBEC` varchar(33) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `PEOPLE`
--

INSERT INTO `PEOPLE` (`ID`, `PRIEZVISKO`, `MENO`, `EMAIL`, `SKOLA`, `SKOLA_ADR`, `ULICA`, `PSC`, `OBEC`) VALUES
(1, 'Abrahám', 'Štefan', 'email1@gmail.com', 'Gymnázium Martina Kukučína', 'Revúca, V. Clementisa 1166/21, 05001', 'Revúcka Lehota 1', 4918, 'Revúcka Lehota'),
(2, 'Adamec', 'Albert', 'email2@gmail.com', 'Stredná odborná škola strojnícka', 'Považská Bystrica, Športovcov 341/2, 01749', 'Sládkovičova 1', 1701, 'Považská Bystrica'),
(3, 'Andrášová', 'Adela', 'email3@gmail.com', 'Súkromná stredná odborná škola', 'Poprad, Ul. 29. augusta 4812, 05801', 'Tatranská Lomnica 1', 5960, 'Tatranská Lomnica'),
(4, 'Baran', 'Adrián', 'email4@gmail.com', 'Stredná priemyselná škola elektrotechnická', 'Bratislava-Petržalka, Hálova 16, 85101', 'Bartókova 1', 81102, 'Bratislava'),
(5, 'Bašková', 'Eunika', 'email5@gmail.com', 'Gymnázium arm. gen. Ludvíka Svobodu', 'Humenné, Komenského 4, 06601', 'Partizánska 1', 6601, 'Humenné'),
(6, 'Beňová', 'Ester', 'email6@gmail.com', 'Gymnázium Antona Bernoláka', 'Námestovo, Ulica mieru 307/23, 02901', 'Novoť 1', 2955, 'Novoť'),
(7, 'Bernolák', 'Ľubomír', 'email7@gmail.com', 'Stredná priemyselná škola elektrotechnická', 'Bratislava-Petržalka, Hálova 16, 85101', 'Wolkrova 1', 85101, 'Bratislava - Petržalka'),
(8, 'Bernolák', 'Norbert', 'email8@gmail.com', 'Gymnázium', 'Myjava, Jablonská 5, 90701', 'Poriadie 1', 90622, 'Poriadie'),
(9, 'Bilek', 'Jozef', 'email9@gmail.com', 'Stredná priemyselná škola', 'Dubnica nad Váhom, Obrancov mieru 343/1, 01841', 'Neporadza 1', 91326, 'Neporadza'),
(10, 'Boreková', 'Iveta', 'email10@gmail.com', 'Stredná odborná škola polytechnická', 'Humenné, Štefánikova 1550/20, 06601', 'Puškinova 1', 9303, 'Vranov nad Topľou'),
(11, 'Bóriková', 'Enna', 'email11@gmail.com', 'Gymnázium', 'Vranov nad Topľou, Dr. C. Daxnera 88/3, 09380', 'Lúčna 1', 9301, 'Vranov nad Topľou'),
(12, 'Cesnaková', 'Natália', 'email12@gmail.com', 'Stredná priemyselná škola elektrotechnická', 'Bratislava-Petržalka, Hálova 16, 85101', 'Hostáď 1', 93036, 'Čečínska Potôň'),
(13, 'Dávidová', 'Karina', 'email13@gmail.com', 'Stredná priemyselná škola elektrotechnická', 'Bratislava-Petržalka, Hálova 16, 85101', 'Dolná 1', 90051, 'Zohor'),
(14, 'Debnárová', 'Alana', 'email14@gmail.com', 'Spojená škola-Stredná odborná škola technická', 'Prešov, Ľ. Podjavorinskej 22, 08005', 'L. Novomeského 1', 8001, 'Prešov'),
(15, 'Devečka', 'Koloman', 'email15@gmail.com', 'Stredná odborná škola obchodu a služieb', 'Nové Mesto nad Váhom, Piešťanská 2262/80, 91501', 'Ivanovce 1', 91305, 'Ivanovce'),
(16, 'Doležalová', 'Zoja', 'email16@gmail.com', 'Gymnázium', 'Považská Bystrica, Školská 234/8, 01701', 'Riečna 1', 2001, 'Hrabovka'),
(17, 'Dorko', 'Tibor', 'email17@gmail.com', 'Gymnázium Ľudovíta Štúra', 'Zvolen, Hronská 1467/3, 96049', 'Neresnická 1', 96261, 'Dobrá Niva'),
(18, 'Droppa', 'Miloslav', 'email18@gmail.com', 'Gymnázium', 'Vranov nad Topľou, Dr. C. Daxnera 88/3, 09380', 'Vyšný Žipov 1', 9433, 'Vyšný Žipov'),
(19, 'Dudiková', 'Iveta', 'email19@gmail.com', 'Stredná priemyselná škola elektrotechnická', 'Bratislava-Petržalka, Hálova 16, 85101', 'Hraničiarska 1', 85110, 'Bratislava - Čunovo'),
(20, 'Ďuricová', 'Olívia', 'email20@gmail.com', 'Gymnázium Pavla Horova', 'Michalovce, Masarykova 1, 07179', 'Hradská 1', 7215, 'Budkovce'),
(21, 'Ďurka', 'Svetozár', 'email21@gmail.com', 'Stredná priemyselná škola elektrotechnická', 'Bratislava-Petržalka, Hálova 16, 85101', 'Jasovská 1', 85107, 'Bratislava'),
(22, 'Ďurová', 'Jela', 'email22@gmail.com', 'Gymnázium', 'Vranov nad Topľou, Dr. C. Daxnera 88/3, 09380', 'Puškinova 1', 9303, 'Čemerné'),
(23, 'Fajnor', 'Bohumil', 'email23@gmail.com', 'Gymnázium', 'Stropkov, Konštantínova 64, 09101', 'Breznica 1', 9101, 'Stropkov'),
(24, 'Gál', 'Marcel', 'email24@gmail.com', 'Gymnázium', 'Vranov nad Topľou, Dr. C. Daxnera 88/3, 09380', 'Majerovce 1', 9409, 'Majerovce'),
(25, 'Gavenda', 'Bohuš', 'email25@gmail.com', 'Gymnázium Ľudovíta Štúra', 'Zvolen, Hronská 1467/3, 96049', 'Ľ. Štúra 1', 96001, 'Zvolen'),
(26, 'Gavendová', 'Sláva', 'email26@gmail.com', 'Gymnázium Milana Rúfusa', 'Žiar nad Hronom, J. Kollára 2, 96501', 'Janova Lehota 1', 96624, 'Janova Lehota'),
(27, 'Greguš', 'Mikuláš', 'email27@gmail.com', 'Spojená škola cirkevná-Gymnázium sv. Cyrila a Metoda', 'Humenné, Duchnovičova 24, 06601', 'Ľubiša 1', 6711, 'Ľubiša'),
(28, 'Habdušová', 'Elvíra', 'email28@gmail.com', 'Gymnázium', 'Bratislava-Ružinov, Metodova 2, 82108', 'Konopná 1', 90025, 'Čierna Voda'),
(29, 'Hagarová', 'Margita', 'email29@gmail.com', 'Gymnázium', 'Ružomberok, Š. Moyzesa 21, 03401', 'Vavra Šrobára 1', 3401, 'Ružomberok'),
(30, 'Haňušková', 'Blanka', 'email30@gmail.com', 'Stredná priemyselná škola elektrotechnická', 'Bratislava-Petržalka, Hálova 16, 85101', 'Parková 1', 90042, 'Alžbetin Dvor'),
(31, 'Harvánek', 'Ervín', 'email31@gmail.com', 'Stredná priemyselná škola strojnícka', 'Bratislava-Staré Mesto, Fajnorovo nábrežie 5, 81475', 'Stromová 1', 90101, 'Malacky'),
(32, 'Hlaváčiková', 'Petronela', 'email32@gmail.com', 'Stredná odborná škola sv. Jozefa Robotníka', 'Žilina, Saleziánska 18, 01001', 'Družstevná 1', 1004, 'Bánová'),
(33, 'Hoza', 'Mário', 'email33@gmail.com', 'Gymnázium', 'Snina, Študentská 4, 06901', 'Parková 1', 6761, 'Stakčín'),
(34, 'Husťák', 'Miroslav', 'email34@gmail.com', 'Gymnázium', 'Myjava, Jablonská 5, 90701', 'SNP 1', 91601, 'Stará Turá'),
(35, 'Janček', 'Félix', 'email35@gmail.com', 'Spojená škola-Stredná priemyselná škola elektrotechnická S. A. Jedlika', 'Nové Zámky, Komárňanská 28, 94075', 'Zemné 1', 94122, 'Zemné'),
(36, 'Jantošovič', 'Karol', 'email36@gmail.com', 'Spojená škola-Gymnázium Jura Hronca', 'Bratislava-Ružinov, Novohradská 3, 82109', 'Suché Miesto 1', 90025, 'Chorvátsky Grob'),
(37, 'Junás', 'Oldrich', 'email37@gmail.com', 'Stredná priemyselná škola', 'Dubnica nad Váhom, Obrancov mieru 343/1, 01841', 'Farská 1', 1861, 'Beluša'),
(38, 'Kolár', 'Teodor', 'email38@gmail.com', 'Stredná priemyselná škola elektrotechnická', 'Prešov, Plzenská 1, 08047', 'Šarišské Jastrabie 1', 6548, 'Šarišské Jastrabie'),
(39, 'Kolník', 'Teodor', 'email39@gmail.com', 'Spojená škola-Gymnázium Jura Hronca', 'Bratislava-Ružinov, Novohradská 3, 82109', 'Štefana Králika 1', 84108, 'Bratislava'),
(40, 'Krajčová', 'Silvia', 'email40@gmail.com', 'Stredná odborná škola elektrotechnická', 'Poprad, Hlavná 1400/1, 05951', 'Vlkovce 1', 5971, 'Vlkovce'),
(41, 'Kubecová', 'Drahoslava', 'email41@gmail.com', 'Gymnázium Matky Alexie', 'Bratislava-Staré Mesto, Jesenského 4/A, 81102', 'A.Dubčeka 1', 90301, 'Senec'),
(42, 'Martinek', 'Martin', 'email42@gmail.com', 'Súkromná stredná odborná škola podnikania', 'Senica, Hollého 1380, 90501', 'J.Mudrocha 1', 90501, 'Sotina'),
(43, 'Maslo', 'Svätopluk', 'email43@gmail.com', 'Gymnázium Ľudovíta Štúra', 'Zvolen, Hronská 1467/3, 96049', 'Centrum 1', 96001, 'Zvolen'),
(44, 'Melicher', 'Beňadik', 'email44@gmail.com', 'Gymnázium Hansa Selyeho s vyučovacím jazykom maďarským', 'Komárno, Biskupa Királya 5, 94501', 'Veľké Ludince 1', 93565, 'Veľké Ludince'),
(45, 'Michalko', 'Peter', 'email45@gmail.com', 'Elektrotechnicka a stavebna škola \'Nikola Tesla\'', 'Narodnog Fronta 1, 23000 Zrenjanin', 'Ive Lole Ribara 1', 26215, 'Padina'),
(46, 'Mrazko', 'Bystrík', 'email46@gmail.com', 'Spojená škola sv. Františka z Assisi-Gymnázium sv. Františka z Assisi', 'Bratislava-Karlova Ves, Karloveská 32, 84104', 'Ľudovíta Fullu 1', 84105, 'Bratislava'),
(47, 'Novák', 'Zdenko', 'email47@gmail.com', 'Stredná odborná škola technická', 'Šurany, Nitrianska 61, 94201', 'Lipová 1', 94110, 'Tvrdošovce'),
(48, 'Palo', 'Roman', 'email48@gmail.com', 'Stredná priemyselná škola elektrotechnická', 'Bratislava-Petržalka, Hálova 16, 85101', 'Kostolná pri Dunaji 1', 90301, 'Kostolná pri Dunaji'),
(49, 'Paľov', 'Róbert', 'email49@gmail.com', 'Stredná priemyselná škola elektrotechnická', 'Bratislava-Petržalka, Hálova 16, 85101', 'Kadnárova 1', 83151, 'Bratislava - Rača'),
(50, 'Paulik', 'Viliam', 'email50@gmail.com', 'Stredná priemyselná škola', 'Dubnica nad Váhom, Obrancov mieru 343/1, 01841', 'Dolná Súča 1', 91332, 'Dolná Súča'),
(51, 'Pavliček', 'Severín', 'email51@gmail.com', 'Stredná odborná škola informačných technológií', 'Bratislava-Rača, Hlinícka 1, 83152', 'Sokolská 1', 90872, 'Závod'),
(52, 'Pavúk', 'Belo', 'email52@gmail.com', 'Gymnázium Ivana Horvátha', 'Bratislava-Ružinov, Ivana Horvátha 14, 82103', 'Lotyšská 1', 82106, 'Bratislava - Podunajské Biskupice'),
(53, 'Petruška', 'Marek', 'email53@gmail.com', 'Stredná priemyselná škola', 'Bardejov, Komenského 5, 08542', 'Vaniškovce 1', 8641, 'Vaniškovce'),
(54, 'Šimko', 'Viktor', 'email54@gmail.com', 'Spojená škola-Športové gymnázium', 'Nitra, Slančíkovej 2, 95050', 'Narcisová 1', 94901, 'Nitra'),
(55, 'Škantár', 'Blažej', 'email55@gmail.com', 'Stredná odborná škola technická', 'Michalovce, Partizánska 1, 07192', 'Jovsa 1', 7232, 'Jovsa'),
(56, 'Višňovský', 'Dobroslav', 'email56@gmail.com', 'Spojená škola-Športové gymnázium', 'Nitra, Slančíkovej 2, 95050', 'Chmeľová dolina 1', 94901, 'Zobor');

-- --------------------------------------------------------

--
-- Table structure for table `PERFORMANCE`
--

CREATE TABLE `PERFORMANCE` (
  `ID` int(11) NOT NULL,
  `ID_ROUTES` int(11) NOT NULL,
  `START` time NOT NULL,
  `FINISH` time NOT NULL,
  `TIME` time NOT NULL,
  `DATE` date DEFAULT NULL,
  `DISTANCE` int(11) NOT NULL,
  `AVG_SPEED` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci;

--
-- Dumping data for table `PERFORMANCE`
--

INSERT INTO `PERFORMANCE` (`ID`, `ID_ROUTES`, `START`, `FINISH`, `TIME`, `DATE`, `DISTANCE`, `AVG_SPEED`) VALUES
(1, 12, '06:00:00', '10:05:00', '04:05:00', '2018-01-01', 0, 0),
(2, 12, '01:00:00', '02:00:00', '01:00:00', '2019-04-04', 50, 14),
(3, 28, '01:02:00', '03:02:00', '02:00:00', '2018-01-01', 50, 7);

-- --------------------------------------------------------

--
-- Table structure for table `ROLES`
--

CREATE TABLE `ROLES` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(15) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ROLES`
--

INSERT INTO `ROLES` (`ID`, `NAME`) VALUES
(1, 'administrator'),
(2, 'user'),
(3, 'unregistred');

-- --------------------------------------------------------

--
-- Table structure for table `ROUTES`
--

CREATE TABLE `ROUTES` (
  `ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `START_PLACE` text COLLATE utf8_unicode_ci NOT NULL,
  `END_PLACE` text COLLATE utf8_unicode_ci NOT NULL,
  `MODE` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DISTANCE` double DEFAULT NULL,
  `ROUTE_MODE` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `START_TIME` time DEFAULT NULL,
  `END_TIME` time DEFAULT NULL,
  `DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `STATUS` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RATING` int(11) DEFAULT NULL,
  `NOTE` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ROUTES`
--

INSERT INTO `ROUTES` (`ID`, `USER_ID`, `START_PLACE`, `END_PLACE`, `MODE`, `DISTANCE`, `ROUTE_MODE`, `START_TIME`, `END_TIME`, `DATE`, `STATUS`, `RATING`, `NOTE`) VALUES
(10, 15, 'Bratislava, Slovakia', 'samorin', 'DRIVING', 27, 'private', '00:00:00', '00:00:00', '2018-05-08 05:05:57', 'done', 0, 'hotovo :)'),
(11, 14, 'Kalinkovo, Slovakia', 'Å amorÃ­n, Slovakia', 'WALKING', 35, 'private', '00:00:00', '00:00:00', '2018-05-08 05:53:08', 'done', 4, ''),
(12, 16, 'KoÅ¡ice, Slovakia', 'Bratislava, Slovakia', 'DRIVING', 350, 'private', '00:00:00', '02:00:00', '2018-05-17 18:52:23', 'inactive', 5, 'asd'),
(18, 15, 'Kalinkovo, Slovakia', 'GabÄÃ­kovo, Slovakia', 'WALKING', 5, 'private', '00:00:00', '16:06:00', '2018-05-12 18:39:51', 'active', 2, ''),
(20, 14, 'ÄŒimhovÃ¡, Slovakia', 'Bratislava', 'DRIVING', 0, 'private', '00:00:00', '00:00:00', '2018-05-07 15:38:44', 'inactive', 0, ''),
(21, 14, 'KoÅ¡ice, Slovakia', 'Zvolen', 'DRIVING', 0, 'private', '00:00:00', '00:00:00', '2018-05-07 15:39:43', 'inactive', 0, ''),
(22, 14, 'Å amorÃ­n, Slovakia', 'Cimhova', 'DRIVING', 0, 'private', '00:00:00', '00:00:00', '2018-05-08 05:45:20', 'inactive', 0, ''),
(23, 14, 'SliaÄ, Slovakia', 'Bratislava, Slovakia', 'DRIVING', 0, 'private', '00:00:00', '00:00:00', '2018-05-08 05:45:20', 'inactive', 0, ''),
(25, 14, 'Bratislava, Slovakia', 'cimhova', 'DRIVING', 40, 'private', '00:00:00', '00:00:00', '2018-05-08 05:54:27', 'inactive', 3, ''),
(26, 14, 'Bratislava, Slovakia', 'cimhova', 'DRIVING', 197, 'private', '00:00:00', '00:00:00', '2018-05-08 05:53:11', 'inactive', 4, ''),
(27, 14, 'Å amorÃ­n, Slovakia', 'senec, s', 'DRIVING', 0, 'private', '00:00:00', '00:00:00', '2018-05-08 06:08:29', 'inactive', 0, ''),
(28, 16, 'Bratislava, Slovakia', 'lamac', 'DRIVING', 100, 'public', '00:00:00', '03:02:00', '2018-05-17 18:52:23', 'active', 3, ''),
(48, 17, 'Bratislava, Slovakia', 'NItra', 'DRIVING', 0, 'public', '00:00:00', '00:00:00', '2018-05-17 00:27:16', 'inactive', 0, ''),
(49, 17, 'Kosice', 'Presov', 'WALKING', 0, 'public', '00:00:00', '00:00:00', '2018-05-17 16:05:50', 'inactive', 0, ''),
(50, 18, 'Habovka', 'Zuberec', 'DRIVING', 0, 'private', '00:00:00', '00:00:00', '2018-05-17 00:36:09', 'done', 0, ''),
(51, 18, 'Kosice', 'Bratislava', 'WALKING', 217, 'public', '00:00:00', '00:00:00', '2018-05-17 01:47:45', 'done', 5, ''),
(52, 17, 'TrebiÅ¡ov, Slovakia', 'Trencin', 'DRIVING', 0, 'public', '00:00:00', '00:00:00', '2018-05-17 16:05:50', 'active', 0, ''),
(53, 0, '', '', '', 0, '', '00:00:00', '00:00:00', '2018-05-18 20:01:42', 'active', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `TEAMS`
--

CREATE TABLE `TEAMS` (
  `ID` int(11) NOT NULL,
  `TEAMNUMBER` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `ROUTE_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci;

--
-- Dumping data for table `TEAMS`
--

INSERT INTO `TEAMS` (`ID`, `TEAMNUMBER`, `USER_ID`, `ROUTE_ID`) VALUES
(28, 1, 17, NULL),
(29, 1, 18, NULL),
(30, 1, 19, NULL),
(31, 6, 16, NULL),
(32, 6, 20, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `USERS`
--

CREATE TABLE `USERS` (
  `ID` int(11) NOT NULL,
  `EMAIL` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `FIRSTNAME` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `LASTNAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ROLE_ID` int(11) NOT NULL,
  `PASSWORD` text COLLATE utf8_unicode_ci NOT NULL,
  `REGISTRATION` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ACTIVATION_HASH` text COLLATE utf8_unicode_ci NOT NULL,
  `SCHOOL` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SCHOOL_ADDR` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STREET` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CITY` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NEWSLETTER` tinyint(4) NOT NULL DEFAULT '0',
  `ACTIVATE` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `USERS`
--

INSERT INTO `USERS` (`ID`, `EMAIL`, `FIRSTNAME`, `LASTNAME`, `ROLE_ID`, `PASSWORD`, `REGISTRATION`, `ACTIVATION_HASH`, `SCHOOL`, `SCHOOL_ADDR`, `STREET`, `CITY`, `NEWSLETTER`, `ACTIVATE`) VALUES
(14, 'example@example.com', 'jano', 'jano', 2, '2830b298820baee5a575b3f8625ba7367cd229312e83d08b6d0093001c25fd5b', '2018-05-01 10:32:19', '', '', '', '', '', 0, 0),
(15, 'admin@example.com', 'admin', 'admin', 1, '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '2018-05-05 06:01:40', '', '', '', '', '', 0, 0),
(16, 'user@example.com', 'user', 'user', 2, '04f8996da763b7a969b1028ee3007569eaf3a635486ddab211d512c85b9df8fb', '2018-05-18 20:28:30', 'c0c7c76d30bd3dcaefc96f40275bdc0a', '', '', '', '', 0, 1),
(17, 'jano@example.com', 'jano', 'example', 1, '56b1db8133d9eb398aabd376f07bf8ab5fc584ea0b8bd6a1770200cb613ca005', '2018-05-16 23:55:32', '6e2713a6efee97bacb63e52c54f0ada0', '', '', '', '', 0, 0),
(18, 'jaro@hagara.com', 'jaro', 'hagara', 2, '56b1db8133d9eb398aabd376f07bf8ab5fc584ea0b8bd6a1770200cb613ca005', '2018-05-17 00:35:42', '9766527f2b5d3e95d4a733fcfb77bd7e', '', '', '', '', 0, 0),
(19, 'fero@yuki.com', 'fero', 'yuki', 2, '56b1db8133d9eb398aabd376f07bf8ab5fc584ea0b8bd6a1770200cb613ca005', '2018-05-17 16:16:59', '22fb0cee7e1f3bde58293de743871417', '', '', '', '', 0, 0),
(20, 'milan@verne.com', 'milan', 'verne', 2, '56b1db8133d9eb398aabd376f07bf8ab5fc584ea0b8bd6a1770200cb613ca005', '2018-05-17 17:17:03', '06138bc5af6023646ede0e1f7c1eac75', '', '', '', '', 0, 0),
(34, 'nieco@example', 'nieco', 'nieco', 2, '56b1db8133d9eb398aabd376f07bf8ab5fc584ea0b8bd6a1770200cb613ca005', '2018-05-18 19:55:13', 'aa169b49b583a2b5af89203c2b78c67c', '', '', '', '', 0, 0),
(35, 'cnkanokca@example.com', 'nieco', 'nieco', 2, '56b1db8133d9eb398aabd376f07bf8ab5fc584ea0b8bd6a1770200cb613ca005', '2018-05-18 19:55:54', '3dc4876f3f08201c7c76cb71fa1da439', '', '', '', '', 0, 0),
(36, 'janci@example.com', 'janci', 'example', 2, '56b1db8133d9eb398aabd376f07bf8ab5fc584ea0b8bd6a1770200cb613ca005', '2018-05-18 20:35:11', 'c51ce410c124a10e0db5e4b97fc2af39', '', '', '', '', 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ACTUALITIES`
--
ALTER TABLE `ACTUALITIES`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `PERFORMANCE`
--
ALTER TABLE `PERFORMANCE`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ROLES`
--
ALTER TABLE `ROLES`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ROUTES`
--
ALTER TABLE `ROUTES`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `TEAMS`
--
ALTER TABLE `TEAMS`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `USERS`
--
ALTER TABLE `USERS`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ACTUALITIES`
--
ALTER TABLE `ACTUALITIES`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `PERFORMANCE`
--
ALTER TABLE `PERFORMANCE`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ROLES`
--
ALTER TABLE `ROLES`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ROUTES`
--
ALTER TABLE `ROUTES`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `TEAMS`
--
ALTER TABLE `TEAMS`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `USERS`
--
ALTER TABLE `USERS`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
