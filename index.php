<?php
	include ('View/Blade/head.blade.php');
?>

<main role="main">
	<div class="jumbotron">
		<div class="container">
			<?php if(isset($_SESSION['login_user'])): ?>
				<?php echo '<h1 class="display-3">Hello, '.$_SESSION['login_user'].'</h1>'; ?>
				<p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
				<p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>
			<?php else: ?>
				<?php echo '<h1 class="display-3">Hello, Stranger. Please log in.</h1>'; ?>
				<p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
				<p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>
				<?php include ('View/Sessions/guest.view.php'); ?>
			<?php endif; ?>
		</div>
	</div>
	<hr>
</main>

<?php
	include ('View/Blade/footer.blade.php');
	echo $_SERVER['DOCUMENT_ROOT'].'/Model/User.php';
?>
