var start;
var end;
var selectedMode;
var routeMode;

function createRoute() {
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "/BusinessLogic/createRoute.php", false);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("placeStart="+start+"&placeEnd="+end+"&mode="+routeMode+"&routeMode="+routeShare);
		console.log("tutu");
    window.location = '/View/Routes/routes.view.php';
}

function myMap() {
	var map = new google.maps.Map(document.getElementById('map'), {
		center: {lat: 48.662, lng: 19.700},
		zoom: 8
	});

	var card = document.getElementById('card');
	var start = document.getElementById('start');
	var end = document.getElementById('end');

	var directionsDisplay = new google.maps.DirectionsRenderer;
	var directionsService = new google.maps.DirectionsService;
	directionsDisplay.setMap(map);
	directionsDisplay.setPanel(document.getElementById('right-panel'));

	card.style.display = 'block';
	map.controls[google.maps.ControlPosition.TOP_LEFT].push(card);

	var autocomplete_start = new google.maps.places.Autocomplete(start);
	var autocomplete_end = new google.maps.places.Autocomplete(end);

	// Bind the map's bounds (viewport) property to the autocomplete object,
	// so that the autocomplete requests use the current map bounds for the
	// bounds option in the request.
	autocomplete_start.bindTo('bounds', map);
	autocomplete_end.bindTo('bounds', map);

	var onChangeHandler = function() {
		calculateAndDisplayRoute(directionsService, directionsDisplay);
	};

	document.getElementById('start').addEventListener('change', onChangeHandler);
	document.getElementById('end').addEventListener('change', onChangeHandler);
	document.getElementById('mode').addEventListener('change', onChangeHandler);
	document.getElementById('route-share').addEventListener('change', onChangeHandler);
}

function calculateAndDisplayRoute(directionsService, directionsDisplay) {
	start = document.getElementById('start').value;
	end = document.getElementById('end').value;
	routeMode = document.getElementById('mode').value;
	routeShare = document.getElementById('route-share').value;

	if (start.length > 0 && end.length > 0) {
		directionsService.route({
			origin: start,
			destination: end,
			travelMode: google.maps.TravelMode[routeMode]
		}, function(response, status) {
			if (status === 'OK') {
				directionsDisplay.setDirections(response);
			} else {
				window.alert('Directions request failed due to ' + status);
			}
		});
	}
}
