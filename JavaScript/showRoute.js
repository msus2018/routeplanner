var start;
var end;
var selectedMode;
//get Route ID
var url  = window.location.href;
var index = url.indexOf("id");
id = url.substring(index, url.length);
var id;

function myMap() {
	var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -33.8688, lng: 151.2195},
        zoom: 10
    });

    var directionsDisplay = new google.maps.DirectionsRenderer;
    var directionsService = new google.maps.DirectionsService;
    directionsDisplay.setMap(map);

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            //var mapData = JSON.parse(this.responseJSON);
            console.log(this.responseText);
        }
    };
    xmlhttp.open("GET", "/BusinessLogic/getRoute.php?id="+id, true);
    xmlhttp.send();

    var onChangeHandler = function() {
        calculateAndDisplayRoute(directionsService, directionsDisplay);
    };

    // document.getElementById('start').addEventListener('change', onChangeHandler);
    // document.getElementById('end').addEventListener('change', onChangeHandler);
    // document.getElementById('mode').addEventListener('change', onChangeHandler);
}

function calculateAndDisplayRoute(directionsService, directionsDisplay) {
    directionsService.route({
        origin: start,
        destination: end,
        travelMode: google.maps.TravelMode[selectedMode]
        }, function(response, status) {
            if (status === 'OK') {
                    directionsDisplay.setDirections(response);
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });
}