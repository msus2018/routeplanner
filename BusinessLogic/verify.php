<?php
session_start();

include ($_SERVER['DOCUMENT_ROOT'].'/Model/User.php');

if(isset($_GET['email']) && !empty($_GET['email']) AND isset($_GET['hash']) && !empty($_GET['hash'])){
    // Verify data
    $email = $_GET['email'];
    $hash = $_GET['hash'];
    
    $user = User::findByEmail($email);

    if ($hash == $user->getHash()) {
    	if (User::isActivated($email)) {
    		header('location: /');
    	}
    	else {
    		User::activate($email);
    		header('location: /View/Sessions/activate.view.php');
    	}
    }

}
?>