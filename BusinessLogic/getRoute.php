<?php
session_start();
include ($_SERVER['DOCUMENT_ROOT'].'/Model/Route.php');
include ($_SERVER['DOCUMENT_ROOT'].'/Model/User.php');

if (!isset($_SESSION['login_user'])) {
	header("location: /");
}

$route = Route::find($_GET['id']);
$user = User::findByEmail($_SESSION['login_user']);

if ($route->getRouteMode() == "private" && $route->getUser() != $user->getId() && User::isAdmin($user->getEmail()) == 0) {
	header("location: /");
}

$result->start = $route->getStartPlace();
$result->end = $route->getEndPlace();
$result->mode = $route->getMode();

$json = json_encode($result);

echo $user->getId();
?>