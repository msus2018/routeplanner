<?php
session_start();
include ($_SERVER['DOCUMENT_ROOT'].'/Model/User.php');

if (!User::isAdmin($_SESSION['login_user'])) {
	header('location: /');
}
?>