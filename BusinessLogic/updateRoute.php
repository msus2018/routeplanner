<?php
session_start();
include ($_SERVER['DOCUMENT_ROOT'].'/Model/Route.php');
include ($_SERVER['DOCUMENT_ROOT'].'/Model/User.php');
include ($_SERVER['DOCUMENT_ROOT'].'/Model/Stats.php');


if (!isset($_SESSION['login_user'])) {
	header('location: /');
}

$user = User::findByEmail($_SESSION['login_user']);
$route = Route::find($_POST['id']);
$routeId = $route->getId();

if (User::isAdmin($user->getEmail()) == 0 && $route->getRouteMode() == 'private' && $route->getUser() != $user->getId()) {
	header('location: /View/Routes/routes.view.php');

}

if ($route->getStatus() == 'done' || $route->getUser() != $user->getId()) {
	header('location: /View/Routes/routes.view.php');

}

if ($_POST['status'] == 'active') {
	Route::updateAll($user->getId());
}

$a = new DateTime($_POST['end']);
$b = new DateTime($_POST['start']);
$time = $b->diff($a);
$time = $time->format("%H:%I");
echo "<br>tiiiime: ".$time;
echo "<br>start: ".$_POST['start'];
echo "<br>end: ".$_POST['end'];

$pom = explode(":",$time);

$hours = intval($pom[0])*3600;
$minutes = intval($pom[1])*60;
$seconds = ($hours+$minutes);

$distance = $_POST['distance'] - $route->getDistance();

$avg = (intval($distance)*1000)/$seconds;
$avg = round($avg, 2);

Route::update($_POST['id'], $_POST['distance'], $_POST['end'], $_POST['status'], $_POST['rating'], $_POST['note']);
Stats::create($_POST['id'], $distance, $_POST['date'], $_POST['start'], $_POST['end'], $time, $avg);

header('location: /View/Routes/routes.view.php');
?>
