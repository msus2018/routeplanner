<?php
session_start();
include( $_SERVER['DOCUMENT_ROOT'] . '/Model/Actuality.php' );
include( $_SERVER['DOCUMENT_ROOT'] . '/Model/User.php' );

// Create new actuality.
if ( isset( $_POST['createActuality'] ) ) {
    $actuality = new Actuality( $_POST['headline'], $_POST['content'] );
    $actuality->save();
}

// Sign or unsign from newsletter.
if ( isset( $_POST['signNewsletter'] ) ) {
    $status = $_POST['newsletter'];
    User::changeNewsletter( $_SESSION['login_user'], $status );
}


header( 'Location: ' . $_SERVER['HTTP_REFERER'] );
