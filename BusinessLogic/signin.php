<?php
session_start();
include ($_SERVER['DOCUMENT_ROOT'].'/Model/User.php');

$email = $_POST['email'];
$password = $_POST['password'];

$user = User::findByEmail($email);

if (hash("sha256", $password) == $user->getPassword()) {
	if (User::isActivated($email)) {
		$_SESSION['login_user']= $email;
	}
}

header('location: /');
?>