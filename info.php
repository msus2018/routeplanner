<?php
	include ('View/Blade/head.blade.php');
?>

<main role="main">
	<div class="jumbotron">
		<div class="container">
			<h2>Info k projektu</h2>

			<p><b>Členovia:</b>Ján Šefčík, Ján Starý, Matej Sokolík, Dádiv Šerý, Juraj Kurilla</p>

			<p><b>Adresa zadania:</b> http://147.175.98.230/</p>

			<p><b>PhpMyAdmin:</b>http://147.175.98.230/phpmyadmin</p>

			<p>
				<b>Prístupové údaje k databáze:</b>
				Meno: ,
				Heslo: ,
				Nazov DB: RoutePlanning,
				Konfiguračný súbor: config.php
			</p>
			
			<p><b>Export databázy:</b> RoutePlanning.sql</p>

			<p><b>Dodatočná inštalácia:</b>Nebola potrebná žiadna dodatočná inštalácia.</p>

			<h2>Rozdelenie úloh</h2>
			<p>
				<b>Ján Šefčík</b>
				Titulná stránka aplikácia, refaktorizácia kódu. Práca s git repozitátor, ako mergovanie a riešenie konfliktov.
			</p>

			<p>
				<b>Ján Starý</b>
				<p>
					Inicializácia Git, registrácia a verifikácia, riešenie rolí a prístupových práv, definovanie trás. Zabezpečenie aplikácie.
				</p>
			</p>

			<p>
				<b>Matej Sokolík</b>
				<p>
					Definovanie a triedenia trás. Implemetácia týmov, zobrazovanie trás, graficke odlíšenie trás a týmov.
				</p>
			</p>

			<p>
				<b>Dádiv Šerý</b>
				<p>
					Zobrazovanie výsledkov. Implemetácia tabuliek. Generovanie tabuliek do .pdf súboru. Administrátorské rozhranie.
				</p>
			</p>

			<p>
				<b>Juraj Kurilla</b>
				<p>Implementácia odoberanie noviniek, práca z databázov, refakztorizácia kódu.</p>
			</p>
		</div>
	</div>
	<hr>
</main>

<?php
	include ('View/Blade/footer.blade.php');
?>
