<?php
session_start();
include ($_SERVER['DOCUMENT_ROOT'].'/Model/User.php');
?>

<!DOCTYPE html>
<html lang="sk">
<head>
	<meta charset="utf-8">

	<title>Route Plannig</title>

	<link rel="stylesheet" type="text/css" href="../../Style/dashboard.css">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	  <a class="navbar-brand" href="/">Route Planner</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="navbarNav">
	    <ul class="navbar-nav">
	      	<li class="nav-item active">
	        	<a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
	      	</li>
					<li class="nav-item">
	      	<a class="nav-link" href="../../View/Teams/teams.view.php">Teams <span class="sr-only"></span></a>
	      		</li>
	      	<li class="nav-item dropdown" style="z-index: 100">
		      	<a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
		        	Route
		      	</a>
		      	<div class="dropdown-menu">
		        	<a class="dropdown-item" href="../../View/Routes/map.view.php">Create Route</a>
		        	<a class="dropdown-item" href="../../View/Routes/routes.view.php">Routes</a>
		      	</div>
		    </li>
	     	<li class="nav-item">
	          <?php
	          	if (isset($_SESSION['login_user'])) {
	          		$login = $_SESSION['login_user'];

	          		if (User::isAdmin($login) == 1) {
	          			echo '<a class="nav-link" href="../../View/Admin/admin.view.php">Admin</a>';
	          		}
	          	}
	          ?>
	      </li>
				<?php if(isset($_SESSION['login_user'])): ?>
						<li class="nav-item">
								<a class="nav-link" href="../../View/Actualities/actualities.view.php">Actualities</a>
						</li>
				<?php endif; ?>
	    </ul>
	  </div>
	  	<div class="form-inline my-2 my-md-0">
          	<?php
		      	if (isset($_SESSION['login_user'])) {
		      		echo '<a class="btn btn-outline-light" href="../../BusinessLogic/logout.php">Sign Out</a>';
		      	}
		      	else {
		      		echo '<a class="btn btn-outline-light" href="../../View/Sessions/signin.view.php">Sign In</a>';
		      	}
		      ?>
       	</div>
	</nav>
