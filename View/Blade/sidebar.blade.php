<?php

?>
<nav class="col-md-2 d-none d-md-block bg-light sidebar">
  <div class="sidebar-sticky">
    <ul class="nav flex-column">
      <li class="nav-item">
        <a class="nav-link active" href="../../View/Admin/admin.view.php">
	        <span data-feather="home"></span>
	        Dashboard <span class="sr-only">(current)</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../../View/Admin/import.view.php">
          <span data-feather="file"></span>
          Import
        </a>
      </li>
    </ul>
  </div>
</nav>