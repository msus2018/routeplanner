<?php
include ($_SERVER['DOCUMENT_ROOT'].'/View/Blade/head.blade.php');

if (!User::isAdmin($_SESSION['login_user']) == 1) {
	header('location: /');
}
?>

<div class="container-fluid">
  <div class="row">
	  <?php
		include ($_SERVER['DOCUMENT_ROOT'].'/View/Blade/sidebar.blade.php');
		?>
	  <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
	      <h1 class="h2">Dashboard</h1>
	      <div class="btn-toolbar mb-2 mb-md-0">
	      </div>
	    </div>
	  </main>
  </div>
</div>

<?php
include ($_SERVER['DOCUMENT_ROOT'].'/View/Blade/footer.blade.php');
?>
