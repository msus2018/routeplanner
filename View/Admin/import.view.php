<?php
include ($_SERVER['DOCUMENT_ROOT'].'/View/Blade/head.blade.php');
echo $_FILES['uploaded_file'];
if (!User::isAdmin($_SESSION['login_user']) == 1) {
	header('location: /');
}
?>

<div class="container-fluid">
  <div class="row">
	<?php
	include ($_SERVER['DOCUMENT_ROOT'].'/View/Blade/sidebar.blade.php');
	?>
  <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
      <h1 class="h2">Import</h1>
      <div class="btn-toolbar mb-2 mb-md-0">
      </div>
    </div>

	  <div class="container">
	    <form action="../../BusinessLogic/upload.php" method="POST">
	      <div class="input-group">
					<div class="custom-file">
						<label for="exampleFormControlFile1">File input</label>
    				<input type="file" class="form-control-file" id="import" name="import" accept=".csv">

						<!-- <input type="file" class="custom-file-input" id="import" name="import" accept=".csv">
						<label class="custom-file-label" for="inputGroupFile04">Choose file</label> -->
					</div>
					<div class="input-group-append">
						<button class="btn btn-outline-secondary" type="submit">Import</button>
					</div>
				</div>
	    </form>
	  </div>
    </main>
  </div>
</div>

<?php
include ($_SERVER['DOCUMENT_ROOT'].'/View/Blade/footer.blade.php');
?>
