<?php
include ($_SERVER['DOCUMENT_ROOT'].'/View/Blade/head.blade.php');
include ($_SERVER['DOCUMENT_ROOT'].'/Model/Team.php');
//include ($_SERVER['DOCUMENT_ROOT'].'/Model/User.php');


$user = User::findByEmail($_SESSION['login_user']);
$order = "ID";

$teamNumber = $_GET['id'];

if(isset($_POST['email'])){
	//echo "string";
	Team::insertByEmail($teamNumber,$_POST['email']);
}

if (isset($_GET['q'])) {
	$order = $_GET['q'];
	if (!is_array(Team::all($order))) {
		$order = 'ID';
	}
}
if (isset($_GET['rem'])) {
	Team::removeById($_GET['rem']);
}

if (!isset($_SESSION['login_user'])) {
	$teams = Team::findAllInTeam($teamNumber);
}
else if (User::isAdmin($user->getEmail()) == 1) {
	$teams = Team::findAllInTeam($teamNumber);
}
else {
	$teams = Team::findAllInTeam($teamNumber);
	//$publicRoutes = Team::allPublicWithoutUser($user->getId(), $order);
	//$teams = array_merge($teams, $publicRoutes);
}

$counter = 1;

?>
<div class="container-fluid">
	<main role="main">
		<div class="table-responsive-md">
			<table class="table table-hover">
				<caption>Members of team</caption>
				<thead>
				    <tr>
				      	<th scope="col"><a>#</a> </th>
				      	<th scope="col"><a>Meno</a> </th>
				      	<th scope="col"> <a>Priezvisko</a> </th>
				      	<th scope="col"> <a>Email</a> </th>
				      	<?php
				      	if (User::isAdmin($user->getEmail()) == 1) {
				      	echo '<th scope="col"> Update</th>';
				      	}
				      	 ?>
				    </tr>
				  </thead>
				  <tbody>
				  	<?php
				  	foreach ($teams as $route) {
				  		//$user = User::find($route['USER_ID']);
			  			echo '<tr>';
				  		echo '<td>'.$counter.'</td>';
				  		echo '<td>'.$route['FIRSTNAME'].'</td>';
				  		echo '<td>'.$route['LASTNAME'].'</td>';
				  		echo '<td>'.$route['EMAIL'].'</td>';
				  		if (User::isAdmin($user->getEmail()) == 1) {
				  		echo '<td>'.'<a class="btn btn-outline-dark" href="oneTeam.view.php?id='.$route['TEAMNUMBER'].'&rem='.$route['id'].'">Remove from team</a>'.'</td>';
				  		}

				  		$counter++;
				  	}
				  	if (User::isAdmin($user->getEmail()) == 1) {
						echo '<tr>';
						echo '<td colspan = 5>'.'<a style = "text-align:center"class="btn btn-outline-dark" href="addTeamMember.view.php?id='.$route['TEAMNUMBER'].'">Pridaj člena</a>'.'</td>';
						echo '</tr>';
					}



				  	?>
				</tbody>
			</table>
		</div>
		<hr>
	</main>
</div>
