<?php
include ('../Blade/head.blade.php');

include ($_SERVER['DOCUMENT_ROOT'].'/Model/Team.php');
//include ($_SERVER['DOCUMENT_ROOT'].'/Model/User.php');
$user = User::findByEmail($_SESSION['login_user']);
$order = "ID";


if (isset($_GET['q'])) {
	$order = $_GET['q'];
	if (!is_array(Team::all($order))) {
		$order = 'ID';
	}
}
if (!isset($_SESSION['login_user'])) {
	$teams = Team::allTeams();
}
else if (User::isAdmin($user->getEmail()) == 1) {
	$teams = Team::allTeams();
}
else {
	$teams = Team::allTeams();
	//$publicRoutes = Team::allPublicWithoutUser($user->getId(), $order);
	//$teams = array_merge($teams, $publicRoutes);
}

$counter = 1;

?>

<div class="container-fluid">
	<main role="main">
		<div class="table-responsive-md">
			<table class="table table-hover">
				<caption>List of teams</caption>
				<thead>
				    <tr>
				      	<th scope="col"> <a>#</a> </th>
				      	<th scope="col"><a>Team</a> </th>
				      	<th scope="col"> <a>Počet Členov</a> </th>
				      	<th scope="col" > Update</th>
				    </tr>
				  </thead>
				  <tbody>
				  	<?php
				  	foreach ($teams as $route) {
				  		//$user = User::find($route['USER_ID']);
			  			echo '<tr>';
				  		echo '<td>'.$counter.'</td>';
				  		echo '<td>'.$route['TEAMNUMBER'].'</td>';
				  		echo '<td>'.$route['COUNT(*)'].'</td>';
				  		echo '<td><a class="btn btn-outline-dark" href="oneTeam.view.php?id='.$route['TEAMNUMBER'].'">Show</a>';

				  		if (User::isAdmin($user->getEmail()) == 1) {
				  		echo '<a class="btn btn-outline-dark" href="../../BusinessLogic/deleteWholeTeam.php?id='.$route['TEAMNUMBER'].'">Delete</a></td>';
				  		}
				  		echo '</tr>';
				  		$counter++;
				  	}
				  	if (User::isAdmin($user->getEmail()) == 1) {
					echo '<tr>';
						echo '<td colspan = 5>'.'<a style = "text-align:center"class="btn btn-outline-dark" href="addTeam.view.php">Pridaj team</a>'.'</td>';
						echo '</tr>';
					}



				  	?>
				</tbody>
			</table>
		</div>
		<hr>
	</main>
</div>

<?php
	include ('../Blade/footer.blade.php');
?>
