<?php
include ('../Blade/head.blade.php');
include ('../../Model/Route.php');

if (isset($_SESSION['login_user'])) {
	$user = User::findByEmail($_SESSION['login_user']);
}
$order = "ID";

if (isset($_GET['q'])) {
	$order = $_GET['q'];
	if (!is_array(Route::all($order))) {
		$order = 'ID';
	}
}
if (!isset($_SESSION['login_user'])) {
	$routes = Route::allPublic($order);
}
else if (User::isAdmin($user->getEmail()) == 1) {
	$routes = Route::all($order);
}
else {
	$routes = Route::findByUser($user->getId(), $order);
	$publicRoutes = Route::allPublicWithoutUser($user->getId(), $order);
	$routes = array_merge($routes, $publicRoutes);
}

$counter = 1;
?>

<div class="container-fluid">
	<main role="main">
		<div class="table-responsive-md">
			<table class="table table-hover">
				<caption>List of routes</caption>
				<thead>
				    <tr>
				      	<th scope="col"> <a href="routes.view.php?q=ID">#</a> </th>
				      	<th scope="col"><a href="routes.view.php?q=USER_ID">User</a> </th>
				      	<th scope="col"> <a href="routes.view.php?q=START_PLACE">Start</a> </th>
				      	<th scope="col"> <a href="routes.view.php?q=END_PLACE">End</a> </th>
				      	<th scope="col"> <a href="routes.view.php?q=DISTANCE">Distance</a> </th>
				      	<th scope="col"> <a href="routes.view.php?q=MODE">Mode</a> </th>
				      	<th scope="col"> <a href="routes.view.php?q=ROUTE_MODE">Route Mode</a> </th>
				      	<th scope="col"> <a href="routes.view.php?q=STATUS">Status</a> </th>
				      	<th scope="col"> <a href="routes.view.php?q=DATE">Date</a> </th>
				      	<th scope="col"> <a href="routes.view.php?q=RATING">Rating</a> </th>
				      	<th scope="col"> Update</th>
								<th scope='col'></th>
				    </tr>
				  </thead>
				  <tbody>
				  	<?php
				  	foreach ($routes as $route) {
				  		$user = User::find($route['USER_ID']);
				  		if ($route['STATUS'] == 'done') {
				  			echo '<tr class="table-success">';
				  		}
				  		else {
				  			echo '<tr>';
				  		}

				  		echo '<td>'.$counter.'</td>';
				  		echo '<td>'.$user->getEmail().'</td>';
				  		echo '<td>'.$route['START_PLACE'].'</td>';
				  		echo '<td>'.$route['END_PLACE'].'</td>';
				  		echo '<td>'.$route['DISTANCE'].'</td>';
				  		echo '<td>'.$route['MODE'].'</td>';
				  		echo '<td>'.$route['ROUTE_MODE'].'</td>';
				  		echo '<td>'.$route['STATUS'].'</td>';
				  		echo '<td>'.$route['DATE'].'</td>';
				  		echo '<td>'.$route['RATING'].'</td>';
				  		echo '<td>'.'<a class="btn btn-outline-dark" href="route.view.php?id='.$route['ID'].'">Show</a>'.'</td>';
							echo "<td>";
							if($route['USER_ID'] == $user->getId())
							{
									echo '<a class="btn btn-outline-dark" href="details.view.php?id='.$route['ID'].'">Details</a>';
							}
							echo "</td>";
				  		echo '</tr>';
				  		$counter++;
				  	}
				  	?>
				</tbody>
			</table>
		</div>
		<hr>
	</main>
</div>

<?php
include ('../Blade/footer.blade.php');
?>
