<?php
include ('../Blade/head.blade.php');
include ($_SERVER['DOCUMENT_ROOT'].'/Model/Route.php');
include ($_SERVER['DOCUMENT_ROOT'].'/Model/Team.php');

$user = User::findByEmail($_SESSION['login_user']);
$route = Route::find($_GET['id']);

if (!isset($_SESSION['login_user'])) {
	if ($route->getRouteMode() == 'private') {
		header('location: /View/Routes/routes.view.php');
	}
}

if ($route->getId() == null) {
	header('location: /View/Routes/routes.view.php');
}

if (User::isAdmin($user->getEmail()) == 0 && $route->getRouteMode() == 'private' && $route->getUser() != $user->getId()) {
	header('location: /View/Routes/routes.view.php');
}
$data = new \stdClass();
$data->start = $route->getStartPlace();
$data->end = $route->getendPlace();
$data->distance = $route->getDistance();
$data->mode = $route->getMode();
?>

<div class="container-fluid">
	<main role="main">
		<!-- <div class="container-fluid"> -->
			<div class="row">
				<div class="col-sm-6">
					<h2>Route Map</h2>
					<div id="map" style="width:100%;height:500px;"></div>
				</div>

				<div class="col-sm-3">
					<h2>Route info</h2>
					<div id="right-panel" style="width: 100%;height: 500px; overflow: auto;"></div>
				</div>

				<?php
					if ($route->getStatus() != 'done' && ($route->getUser() == $user->getId() || in_array($user->getId(),Team::usersToRoute($_GET['id'])) == 1)) {
						echo '
						<div class="col-sm-3">
							<h2>Update Route</h2>
							<div class="card mx-auto px-4 py-3" style="width: 30rem;">
							  	<div class="card-body">
							    	<form action="../../BusinessLogic/updateRoute.php" method="POST">
										<div class="form-group">
									      <label for="distance">Distance</label>
									      <input type="number" class="form-control" id="distance" name="distance" value="'.$route->getDistance().'" placeholder="Distance..." required="">
									    </div>
											<div class="form-group">
									      <label for="start">Start</label>
									      <input type="time" class="form-control" id="start" name="start" placeholder="Start time...">
									    </div>
									     <div class="form-group">
									      <label for="end">End</label>
									      <input type="time" class="form-control" id="end" name="end" placeholder="End date...">
									    </div>
											<div class="form-group">
									      <label for="date">Date of exercise</label>
									      <input type="date" class="form-control" id="date" name="date" placeholder="Date of exercise...">
									    </div>
									     <div class="form-group">
									    	<label for="firstname">Status</label>
										    <select id="status"  name="status" class="form-control" required="">
										      <option value="active">Active</option>
										      <option value="inactive">Inactive</option>
										      <option value="done">Done</option>
										    </select>
									    </div>
									    <div class="form-group">
									      <label for="rating">Rating</label>
									      <input type="number" class="form-control" id="rating" name="rating" value="'.$route->getRating().'" placeholder="From 1 to 5..." required="">
									    </div>
									    <div class="form-group">
									      	<label for="note">Note</label>
									      	<textarea class="form-control" rows="5" id="comment" value="'.$route->getNote().'" name="note"></textarea>
									    </div>
									    <div class="form-group" hidden="true">
									      	<input type="number" class="form-control" id="id" name="id" value="'.$route->getId().'" >;
									    </div>
									    <button type="submit" class="btn btn-primary">Update</button>
									</form>
							  	</div>
							</div>
						</div>
						';
					}
				?>
			</div>
		</div>

		<div class="container">

		<hr>
	</main>
</div>

<script type="text/javascript">
var mapData = <?php echo json_encode($data) ?>;
function myMap() {
	var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -33.8688, lng: 151.2195},
        zoom: 10
    });

    var directionsDisplay = new google.maps.DirectionsRenderer;
    var directionsService = new google.maps.DirectionsService;
    directionsDisplay.setMap(map);
    directionsDisplay.setPanel(document.getElementById('right-panel'));

    calculateAndDisplayRoute(directionsService, directionsDisplay);

    console.log(mapData);

}

function calculateAndDisplayRoute(directionsService, directionsDisplay) {
    directionsService.route({
        origin: mapData.start,
        destination: mapData.end,
        travelMode: google.maps.TravelMode[mapData.mode]
        }, function(response, status) {
            if (status === 'OK') {
                    directionsDisplay.setDirections(response);
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });
}
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBJ-vFJt_b6FxHbIeuGQuskrYEa1D7V7E8&libraries=places&callback=myMap"></script>

<?php
include ('../Blade/footer.blade.php');
?>
