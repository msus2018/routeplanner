<?php
include ($_SERVER['DOCUMENT_ROOT'].'/View/Blade/head.blade.php');
include ($_SERVER['DOCUMENT_ROOT'].'/Model/Route.php');
include ($_SERVER['DOCUMENT_ROOT'].'/Model/Stats.php');

$user = User::findByEmail($_SESSION['login_user']);
$order = "ID";
$id = $_GET['id'];

if (isset($_GET['q'])) {
    $order = $_GET['q'];
    if (!is_array(Stats::all($id, $order))) {
        $order = 'ID';
    }
}
    $routes = Stats::all($_GET['id'], $order);


$counter = 1;

if(isset($_POST["generate_pdf"]))
{
    require_once('tcpdf/tcpdf.php');
    $obj_pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->SetCreator(PDF_CREATOR);
    $obj_pdf->SetTitle("Generate HTML Table Data To PDF From MySQL Database Using TCPDF In PHP");
    $obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->SetDefaultMonospacedFont('helvetica');
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $obj_pdf->setPrintHeader(false);
    $obj_pdf->setPrintFooter(false);
    $obj_pdf->SetAutoPageBreak(TRUE, 10);
    $obj_pdf->SetFont('helvetica', '', 11);
    $obj_pdf->AddPage();

    $content .= '  
      <table border="1" cellspacing="0" cellpadding="3">  
           <tr>
                         <th># </th>
                         <th>User </th>
                         <th>Start </th>
                         <th>End</th>
                         <th>Mode</th>
                         <th>Status </th>
                         <th>Date </th>
                         <th>Rating</th>
                         <th>Beginning</th>
                         <th>Finish</th>
                         <th>Duration</th>
                         <th>DISTANC</th>
                         <th>Average speed (m/s)</th>
                    </tr>  
      ';
                foreach ($routes as $route) {
                    $user = User::find($route['USER_ID']);

                    $content .= '
                        <tr>
                            <td>' . $counter . '</td>
                            <td>' . $user->getEmail() . '</td>
                            <td>' . $route["START_PLACE"] . '</td>
                            <td>' . $route["END_PLACE"] . '</td>
                            <td>' . $route["MODE"] . '</td>
                            <td>' . $route["STATUS"] . '</td>
                            <td>' . $route["DATE"] . '</td>
                            <td>' . $route["RATING"] . '</td>
                            <td>' . $route["START"] . '</td>
                            <td>' . $route["FINISH"] . '</td>
                            <td>' . $route["TIME"] . '</td>
                            <td>' . $route["DISTANCE"] . '</td>
                            <td>' . $route["AVG_SPEED"] . '</td>
                        </tr>
                    ';
                    $counter++;
                }
    $content .= "</tbody></table>";
//    echo $content;
    $obj_pdf->writeHTML($content);
    ob_end_clean();
    $obj_pdf->Output('file.pdf', 'I');
}
?>

    <div class="container-fluid">
        <main role="main">
            <div class="table-responsive-md">
                <form method="post">
                    <input style="margin-left: 90%; margin-top: 10px; margin-bottom: 10px;" type="submit" name="generate_pdf" class="btn btn-success" value="Generate PDF" />
                </form>
                <table class="table table-hover">
                    <caption>List of routes</caption>
                    <thead>
                    <tr>
                        <th scope="col"> <a href="details.view.php?id=12&q=ID">#</a> </th>
                        <th scope="col"> <a href="details.view.php?id=12&q=USER_ID">User</a> </th>
                        <th scope="col"> <a href="details.view.php?id=12&q=START_PLACE">Start</a> </th>
                        <th scope="col"> <a href="details.view.php?id=12&q=END_PLACE">End</a> </th>
                        <th scope="col"> <a href="details.view.php?id=12&q=MODE">Mode</a> </th>
                        <th scope="col"> <a href="details.view.php?id=12&q=STATUS">Status</a> </th>
                        <th scope="col"> <a href="details.view.php?id=12&q=DATE">Date</a> </th>
                        <th scope="col"> <a href="details.view.php?id=12&q=RATING">Rating</a> </th>
                        <th scope="col"> <a href="details.view.php?id=12&q=START">Beginning</a> </th>
                        <th scope="col"> <a href="details.view.php?id=12&q=FINISH">Finish</a> </th>
                        <th scope="col"> <a href="details.view.php?id=12&q=TIME">Duration</a> </th>
                        <th scope="col"> <a href="details.view.php?id=12&q=DISTANCE">DISTANCE</a> </th>
                        <th scope="col"> <a href="details.view.php?id=12&q=AVG_SPEED">Average speed (m/s)</a> </th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    foreach ($routes as $route) {
                        $user = User::find($route['USER_ID']);
                        if ($route['STATUS'] == 'done') {
                            echo '<tr class="table-success">';
                        }
                        else {
                            echo '<tr>';
                        }

                        echo '<td>'.$counter.'</td>';
                        echo '<td>'.$user->getEmail().'</td>';
                        echo '<td>'.$route['START_PLACE'].'</td>';
                        echo '<td>'.$route['END_PLACE'].'</td>';
                        echo '<td>'.$route['MODE'].'</td>';
                        echo '<td>'.$route['STATUS'].'</td>';
                        echo '<td>'.$route['DATE'].'</td>';
                        echo '<td>'.$route['RATING'].'</td>';
                        echo '<td>'.$route['START'].'</td>';
                        echo '<td>'.$route['FINISH'].'</td>';
                        echo '<td>'.$route['TIME'].'</td>';
                        echo '<td>'.$route['DISTANCE'].'</td>';
                        echo '<td>'.$route['AVG_SPEED'].'</td>';
                        echo '</tr>';
                        $counter++;
                    }
                    echo "<tr><td colspan='13'>Average speed during training: ".Stats::avg($id)." m/s</td></tr>"
                    ?>
                    </tbody>
                </table>

            </div>
            <hr>
        </main>
    </div>

<?php
include ($_SERVER['DOCUMENT_ROOT'].'/View/Blade/footer.blade.php');
?>