<?php
include ($_SERVER['DOCUMENT_ROOT'].'/View/Blade/head.blade.php');

if (!isset($_SESSION['login_user'])) {
	header('location: /');
}

?>

<div class="container-fluid">
	<main role="main">

		<div class="card" id="card" style="width: 18rem; opacity: 0.9; ">
		  	<div class="card-body">
		    	<div class="input-group mb-3">
				  	<input type="text" class="form-control" id="start" name="start" placeholder="Start...">
				</div>
				<div class="input-group mb-3">
				  	<input type="text" class="form-control" id="end" name="end" placeholder="End...">
				</div>

				<div class="form-group" style="width: 50%">
			    	<b>Mode of Travel: </b>
				    <select id="mode" class="form-control">
				      <option value="DRIVING">Driving</option>
				      <option value="WALKING">Walking</option>
				      <option value="BICYCLING">Bicycling</option>
				      <option value="TRANSIT">Transit</option>
				    </select>
			    </div>

			    <div class="form-group" style="width: 50%">
			    	<b>Route mode: </b>
				    <select id="route-share" class="form-control">
				      <option value="private">Private</option>
				      <option value="public">Public</option>
				      <option value="relay">Relay</option>
				    </select>
			    </div>
		  	</div>

		  	<button class="btn btn-primary" id="create" onclick="createRoute()">Create</button>
		</div>
		<div class="row">
			<div class="col-sm-9">
					<div id="map" style="width:100%;height:700px;"></div>
				</div>

				<div class="col-sm-3">
					<h2>Route info</h2>
					<div id="right-panel" style="width: 100%;height: 700px; overflow: auto;"></div>
				</div>
		</div>
			<hr>
	</main>
</div>

<script type="text/javascript" src="../../JavaScript/map.js"></script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBJ-vFJt_b6FxHbIeuGQuskrYEa1D7V7E8&libraries=places&callback=myMap"></script>

<?php
include ($_SERVER['DOCUMENT_ROOT'].'/View/Blade/footer.blade.php');
?>
