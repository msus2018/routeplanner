<?php
include ($_SERVER['DOCUMENT_ROOT'].'/View/Blade/head.blade.php');
include ($_SERVER['DOCUMENT_ROOT'].'/Model/Actuality.php');

if (!isset($_SESSION['login_user'])) {
    header('location: /' );
}

?>


<!--TODO
Umozni prihlasit sa na newsletter prihlasenym ludom
Umozni sa odhlasit z newsletteru
Ked vytvorim newsletter tak vsetkym ktory su prihlaseny tak im pride email
-->

<div class="container">
    <div class="row">
        <div style="padding: 20px;">
            <h1>Actualities</h1>
            <?php if(User::isAdmin($login)): ?>
                <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#exampleModal">Add new!</button>
            <?php endif; ?>
        </div>

        <?php foreach(Actuality::all() as $actuality): ?><hr>
            <section style="padding: 20px;width: 100%;">
            <h2><?= $actuality['HEADLINE'] ?></h2> <span style="font-style: italic;">Published on: <?= $actuality['CREATED_AT'] ?></span>
            <p><?= $actuality['CONTENT'] ?></p>
            <hr>
            </section>
        <?php endforeach; ?>
    </div>
</div>
<?php if(User::isAdmin($login)): ?>
    <div class="modal" id="exampleModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form id="form" action="../../BusinessLogic/actualities.php" method="POST">
                    <input type="hidden" name="createActuality">
                <div class="modal-body">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">Headline</label>
                            <div class="col-10">
                                <input class="form-control" name="headline" type="text" value="" id="example-text-input">
                            </div>
                        </div>
                    <div class="form-group">
                        <label for="exampleTextarea">Content</label>
                        <textarea name="content" class="form-control" id="exampleTextarea" rows="3"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="submit" type="submit" class="btn btn-success">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                </form>
            </div>
        </div>
    </div>
<?php endif; ?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form action="../../BusinessLogic/actualities.php" method="POST">
                <input type="hidden" name="signNewsletter">
                <div style="text-align: center;">
                    <?php if(!User::isSignedForNewsletter($_SESSION['login_user'])): ?>
                        <div style="width: 100%">
                            <strong>You are not signed for Newsletter.</strong>
                        </div>
                        <input type="hidden" name="newsletter" value="1">
                        <button type="submit" class="btn btn-sm btn-success">Sign me in!</button>
                    <?php else: ?>
                        <input type="hidden" name="newsletter" value="0">
                        <button type="submit" class="btn btn-sm btn-default">Sign me out!</button>
                    <?php endif; ?>
                </div>
            </form>
        </div>
    </div>
</div>

<?php
include ($_SERVER['DOCUMENT_ROOT'].'/View/Blade/footer.blade.php');
?>
