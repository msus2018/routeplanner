<?php
	include ('../Blade/head.blade.php');
?>

<main role="main">
	<div class="container">
		<div class="card mx-auto px-4 py-3" style="width: 25rem;">
		  <div class="card-body">
		    <form action="../../BusinessLogic/signup.php" method="POST">
			    <div class="form-group">
			      <label for="email">Email address</label>
			      <input type="email" class="form-control" id="email" name="email" placeholder="email@example.com">
			    </div>
			     <div class="form-group">
			      <label for="firstname">Firstname</label>
			      <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Firstname">
			    </div>
			     <div class="form-group">
			      <label for="surename">Surename</label>
			      <input type="text" class="form-control" id="surename" name="surename" placeholder="Surename">
			    </div>
			    <div class="form-group">
			      <label for="password">Password</label>
			      <input type="password" class="form-control" id="password" name="password" placeholder="Password">
			    </div>
			    <div class="form-group">
			      <label for="password_confirmation">Password Confirmation</label>
			      <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Repeat password">
			    </div>
			    <button type="submit" class="btn btn-primary">Sign up</button>
			  </form>
		  </div>
		</div>
		<hr>
	</div>
</main>

<?php
	include ('../Blade/footer.blade.php');
?>
