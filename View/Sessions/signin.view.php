<?php
include ('../Blade/head.blade.php');
?>

<main role="main">
	<div class="container">
		<div class="card mx-auto px-4 py-3" style="width: 25rem;">
		  <div class="card-body">
		    <form action="../../BusinessLogic/signin.php" method="POST">
			    <div class="form-group">
			      <label for="email">Email address</label>
			      <input type="email" class="form-control" id="email" name="email" placeholder="email@example.com">
			    </div>
			    <div class="form-group">
			      <label for="password">Password</label>
			      <input type="password" class="form-control" id="password" name="password" placeholder="Password">
			    </div>
			    <div class="form-check">
			      <input type="checkbox" class="form-check-input" id="dropdownCheck">
			      <label class="form-check-label" for="dropdownCheck">
			        Remember me
			      </label>
			    </div>
			    <button type="submit" class="btn btn-primary">Sign in</button>
			  </form>
			  <div class="dropdown-divider"></div>
			  <a class="dropdown-item" href="signup.view.php">New around here? Sign up</a>
		  </div>
		</div>
		<hr>
	</div>
</main>

<?php
include ('../Blade/footer.blade.php');
?>
