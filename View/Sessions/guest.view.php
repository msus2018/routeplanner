<?php
	$conf = include($_SERVER['DOCUMENT_ROOT'].'/config.php');
	$latlng = array();

	if (isset($_GET['mode'])) {
		$connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name'].";charset=utf8", $conf['username'], $conf['password']);
		$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$stmt = $connection->prepare("SELECT SKOLA_ADR, OBEC FROM PEOPLE WHERE 1");
		$stmt->execute();

		$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
		$data = $stmt->fetchAll();

		foreach ($data as $row) {
				if ($_GET['mode'] == 1) {
					$city = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($row['SKOLA_ADR'])."&key=AIzaSyBJ-vFJt_b6FxHbIeuGQuskrYEa1D7V7E8");
				} else {
					$city = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($row['OBEC'])."&key=AIzaSyBJ-vFJt_b6FxHbIeuGQuskrYEa1D7V7E8");
				}
				$json_City = json_decode($city, true);

				$lattitude = $json_City['results'][0]['geometry']['location']['lat'];
				$longtittude = $json_City['results'][0]['geometry']['location']['lng'];

				$latlng[] = array('lat' => $lattitude, 'lng' => $longtittude);
			}
		}
?>

		<div class="row">
			<div class="btn-group" role="group" aria-label="Basic example">
				Display user's:
				<a class="btn btn-secondary" href="?mode=1" role="button" onclick="window.location.reload()">Schools</a>
				<a class="btn btn-secondary" href="?mode=2" role="button" onclick="window.location.reload()">Cities</a>
			</div>
		</div>

		 <div id="map" style="width:100%;height:700px;"></div>


	<script>
		var latlng = <?php echo json_encode($latlng); ?>;

		function myMap() {
			var map = new google.maps.Map(document.getElementById('map'), {
				center: {lat: 48.662, lng: 19.700},
				zoom: 8
			});

			// Add markers
			for( j = 0; j < latlng.length; j++ ) {
				var position = new google.maps.LatLng(latlng[j]['lat'], latlng[j]['lng']);
					marker = new google.maps.Marker({
					position: position,
					map: map
				});
			}
		}
	</script>

	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBJ-vFJt_b6FxHbIeuGQuskrYEa1D7V7E8&libraries=places&callback=myMap"></script>
