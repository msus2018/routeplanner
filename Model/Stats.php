<?php
$conf = include($_SERVER['DOCUMENT_ROOT'].'/config.php');

/**
 * Created by PhpStorm.
 * User: David
 * Date: 17.5.2018
 * Time: 2:20
 */
class Stats
{
    private $idRoutes;
    private $distance;
    private $startTime;
    private $endTime;
    private $date;
    private $avg;
    private $time;

    private function __construct(
        $time, $avg, $idRoutes, $user, $distance, $startTime, $endTime, $date
    )
    {
        $this->time = $endTime - $startTime;
        $this->avg = $distance / $time;
        $this->idRoutes = $idRoutes;
        $this->distance = $distance;
        $this->startTime = $startTime;
        $this->endTime = $endTime;
        $this->date = $date;
    }
    public function getIdRoutes() {
        return $this->idRoutes;
    }

    public function getDistance() {
        return $this->distance;
    }

    public function getStartTime() {
        return $this->startTime;
    }

    public function getEndTime() {
        return $this->endTime;
    }

    public function getDate() {
        return $this->date;
    }

    public function getTime() {
        return $this->date;
    }

    public function getAvg() {
        return $this->date;
    }

    public static function create(
        $user, $distance, $date, $startTime, $endTime, $time, $avg
    )
    {
        global $conf;

        try {
            $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $values = "(ID_ROUTES, DISTANCE, DATE, START, FINISH, TIME ,AVG_SPEED)";
            $query = "INSERT INTO PERFORMANCE".$values." VALUES ("
                ."'".(int)$user."',"
                ."'".(int)$distance."',"
                ."'".$date."',"
                ."'".$startTime."',"
                ."'".$endTime."',"
                ."'".$time."',"
                ."'".$avg."')";

            $connection->beginTransaction();
            $connection->exec($query);
            $connection->commit();
            $connection = null;
        }
        catch(PDOException $e) {
            $connection->rollback();
            echo "Error: " . $e->getMessage();
        }
    }

    public static function all($id, $order)
    {
        global $conf;

        try {
            $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $connection->prepare("SELECT a.*,b.START_PLACE,b.END_PLACE,b.USER_ID,b.MODE,b.STATUS,b.RATING,b.NOTE FROM PERFORMANCE a JOIN ROUTES b ON a.ID_ROUTES=b.ID WHERE b.ID = ".$id." ORDER BY ".$order." DESC");
            $stmt->execute();

            $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

            return $stmt->fetchAll();
        }
        catch(PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    public static function avg($idRoutes)
    {
        global $conf;

        try {
            $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $connection->prepare("SELECT AVG(AVG_SPEED) AS SPEEDA FROM PERFORMANCE WHERE ID_ROUTES = ". $idRoutes );
            $stmt->execute();

            $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $data = $stmt->fetch();

            foreach ($data as $toto){
                $avg = $toto['SPEEDA'];
            }
            return $avg;
        }
        catch(PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    public static function find($idRoutes)
    {
        global $conf;

        try {
            $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $connection->prepare("SELECT * FROM PERFORMANCE WHERE ID_ROUTES = ". $idRoutes );
            $stmt->execute();

            $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $data = $stmt->fetch();

            $stats = new Stats(
                $data['ID'],
                $data['START'],
                $data['FINISH'],
                $data['TIME'],
                $data['DATE'],
                $data['DISTANCE'],
                $data['AVG_SPEED']
            );

            return $stats;
        }
        catch(PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }
}
