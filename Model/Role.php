<?php
$conf = include($_SERVER['DOCUMENT_ROOT'].'/config.php');

class Role {
	private static $connection;
	private $id;
	private $name;

	private function __construct($id, $name)
	{
		$this->id = $id;
		$this->name = $name;
	}

	public function getId() {
		return $this->id;
	}

	public function getName() {
		return $this->name;
	}

	public static function create($name)
	{
		global $conf;

        try {
            $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['ROLEname'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $values = "(NAME)";
	        $query = "INSERT INTO ROLE".$values." VALUES ("
				."'".$name."')";

			$connection->beginTransaction();
			$connection->exec($query);
			$connection->commit();
			$connection = null;
        }
        catch(PDOException $e) {
            $connection->rollback();
	    	echo "Error: " . $e->getMessage();
        }
	}

	public static function all()
	{
		global $conf;
		
		try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['ROLEname'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("SELECT * FROM ROLE"); 
		    $stmt->execute();

		    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

		    return $stmt->fetchAll();
		}
		catch(PDOException $e) {
		    echo "Error: " . $e->getMessage();
		}
	}

	public static function find($id)
	{
		global $conf;
		
		try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['ROLEname'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("SELECT * FROM ROLE WHERE ID = ". $id ); 
		    $stmt->execute();

		    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
		    $data = $stmt->fetch();

		    $guest = new Guest(
		    	$data['ID'],
		    	$data['FIRSTNAME'],
		    	$data['SURENAME'],
		    	$data['ROLE'],
		    	$data['REGISTRATION']
		    );

		    return $guest;
		}
		catch(PDOException $e) {
		    echo "Error: " . $e->getMessage();
		}
	}

	public static function findByName($name)
	{
		global $conf;
		
		try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['ROLEname'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("SELECT * FROM ROLE WHERE NAME ='".$name."'"); 
		    $stmt->execute();

		    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

		    return $stmt->fetchAll();
		}
		catch(PDOException $e) {
		    echo "Error: " . $e->getMessage();
		}
	}

	public static function deleteById($id)
	{
		global $conf;
		
		try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['ROLEname'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("DELETE * FROM ROLE WHERE ID =".$id); 
		    $stmt->execute();
		}
		catch(PDOException $e) {
		    echo "Error: " . $e->getMessage();
		}
	}

	public static function deleteByName($name)
	{
		global $conf;
		
		try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['ROLEname'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("DELETE * FROM ROLE WHERE NAME =".$name); 
		    $stmt->execute();
		}
		catch(PDOException $e) {
		    echo "Error: " . $e->getMessage();
		}
	}

	public function delete()
	{
		global $conf;
		
		try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['ROLEname'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("DELETE FROM ROLE WHERE id = ". $this->id);
		    $stmt->execute();
		}
		catch(PDOException $e) {
		    echo "Error: " . $e->getMessage();
		}
	}
}

?>