<?php
$conf = include($_SERVER['DOCUMENT_ROOT'].'/config.php');

class Route {
	private static $connection;
	private $id;
	private $user;
	private $startPlace;
	private $endPlace;
	private $mode;
	private $distance;
	private $routeMode;
	private $startTime;
	private $endTime;
	private $date;
	private $status;
	private $rating;
	private $note;

	private function __construct(
		$id, $user, $startPlace, $endPlace, $mode, $distance, $routeMode, $startTime, $endTime, $date, $status, $rating, $note
	)
	{
		$this->id = $id;
		$this->user = $user;
		$this->startPlace = $startPlace;
		$this->endPlace = $endPlace;
		$this->mode = $mode;
		$this->distance = $distance;
		$this->routeMode = $routeMode;
		$this->startTime = $startTime;
		$this->endTime = $endTime;
		$this->date = $date;
		$this->status = $status;
		$this->rating = $rating;
		$this->note = $note;
	}

	public function getId() {
		return $this->id;
	}

	public function getUser() {
		return $this->user;
	}

	public function getStartPlace() {
		return $this->startPlace;
	}

	public function getEndPlace() {
		return $this->endPlace;
	}

	public function getMode() {
		return $this->mode;
	}

	public function getDistance() {
		return $this->distance;
	}

	public function getRouteMode() {
		return $this->routeMode;
	}

	public function getStartTime() {
		return $this->startTime;
	}

	public function getEndTime() {
		return $this->endTime;
	}

	public function getDate() {
		return $this->date;
	}

	public function getStatus() {
		return $this->status;
	}

	public function getRating() {
		return $this->rating;
	}

	public function getNote() {
		return $this->note;
	}

	public static function create(
		$user, $startPlace, $endPlace, $mode, $distance, $routeMode, $startTime, $endTime, $status, $rating, $note
	)
	{
		global $conf;

        try {
            $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $values = "(USER_ID, START_PLACE, END_PLACE, MODE, DISTANCE, ROUTE_MODE, START_TIME, END_TIME, STATUS, RATING, NOTE)";
	        $query = "INSERT INTO ROUTES".$values." VALUES ("
	        	."'".(int)$user."',"
	        	."'".$startPlace."',"
	        	."'".$endPlace."',"
				."'".$mode."',"
				."'".(double)$distance."',"
				."'".$routeMode."',"
				."'".$startTime."',"
				."'".$endTime."',"
				."'".$status."',"
				."'".$rating."',"
				."'".$note."')";

				echo $query;
			$connection->beginTransaction();
			$connection->exec($query);

			$connection->commit();
			$connection = null;
        }
        catch(PDOException $e) {
            $connection->rollback();
	    	echo "Error: " . $e->getMessage();
        }
	}

	public static function all($order)
	{
		global $conf;

		try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("SELECT * FROM ROUTES ORDER BY ".$order);
		    $stmt->execute();

		    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

		    return $stmt->fetchAll();
		}
		catch(PDOException $e) {
		    echo "Error: " . $e->getMessage();
		}
	}

	public static function allPublicWithoutUser($user, $order)
	{
		global $conf;

		try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("SELECT * FROM ROUTES WHERE (ROUTE_MODE = 'public' OR ROUTE_MODE = 'relay') AND NOT USER_ID=".$user." ORDER BY ".$order);
		    $stmt->execute();

		    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

		    return $stmt->fetchAll();
		}
		catch(PDOException $e) {
		    echo "Error: " . $e->getMessage();
		}
	}

	public static function allPublic($order)
	{
		global $conf;

		try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("SELECT * FROM ROUTES WHERE ROUTE_MODE = 'public' OR ROUTE_MODE = 'relay' ORDER BY ".$order);
		    $stmt->execute();

		    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

		    return $stmt->fetchAll();
		}
		catch(PDOException $e) {
		    echo "Error: " . $e->getMessage();
		}
	}


	public static function find($id)
	{
		global $conf;

		try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("SELECT * FROM ROUTES WHERE ID = ". $id );
		    $stmt->execute();

		    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
		    $data = $stmt->fetch();

		    $route = new Route(
		    	$data['ID'],
		    	$data['USER_ID'],
		    	$data['START_PLACE'],
		    	$data['END_PLACE'],
		    	$data['MODE'],
		    	$data['DISTANCE'],
		    	$data['ROUTE_MODE'],
		    	$data['START_TIME'],
		    	$data['END_TIME'],
		    	$data['DATE'],
		    	$data['STATUS'],
		    	$data['RATING'],
		    	$data['NOTE']
		    );

		    return $route;
		}
		catch(PDOException $e) {
		    echo "Error: " . $e->getMessage();
		}
	}

	public static function findByUser($user, $order)
	{
		global $conf;

		try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("SELECT * FROM ROUTES WHERE USER_ID = ".$user." ORDER BY ".$order);
		    $stmt->execute();

		    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

		    return $stmt->fetchAll();

		}
		catch(PDOException $e) {
		    echo "Error: " . $e->getMessage();
		}
	}

	public static function update($id, $distance, $endTime, $status, $rating, $note)
	{
		global $conf;

        try {
            $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	        $query = "UPDATE ROUTES SET"
				.' DISTANCE='.(double)$distance.","
				.' END_TIME='."'".$endTime."',"
				.' STATUS='."'".$status."',"
				.' RATING='.$rating.","
				.' NOTE='."'".$note."'"
				.' WHERE ID='.$id;

			$connection->beginTransaction();
			$connection->exec($query);
			$connection->commit();
			$connection = null;
        }
        catch(PDOException $e) {
            $connection->rollback();
	    	echo "Error: " . $e->getMessage();
        }
	}

	public static function updateAll($user)
	{
		global $conf;

        try {
            $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	        $query = "UPDATE ROUTES SET"
				." STATUS='inactive'"
				.' WHERE USER_ID='.$user." AND STATUS= 'active'";

			$connection->beginTransaction();
			$connection->exec($query);
			$connection->commit();
			$connection = null;
        }
        catch(PDOException $e) {
            $connection->rollback();
	    	echo "Error: " . $e->getMessage();
        }
	}

	public static function deleteById($id)
	{
		global $conf;

		try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("DELETE * FROM ROUTES WHERE ID =".$id);
		    $stmt->execute();
		}
		catch(PDOException $e) {
		    echo "Error: " . $e->getMessage();
		}
	}

	public function delete()
	{
		global $conf;

		try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("DELETE FROM ROUTES WHERE id = ". $this->id);
		    $stmt->execute();
		}
		catch(PDOException $e) {
		    echo "Error: " . $e->getMessage();
		}
	}
}

?>
