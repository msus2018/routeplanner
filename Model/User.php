<?php
$conf = include($_SERVER['DOCUMENT_ROOT'].'/config.php');

class User {
	private static $connection;
	private $id;
	private $email;
	private $firstname;
	private $lastname;
	private $password;
	private $role;
	private $registration;
	private $hash;
	private $school;
	private $school_addr;
	private $street;
	private $city;
	private $newsletter;
	private $activate;

	private function __construct($id, $email, $firstname, $lastname, $password, $role, $registration, $hash, $school, $school_addr, $street, $city, $newsletter = 0, $activate = 0)
	{
		$this->id = $id;
		$this->email = $email;
		$this->firstname = $firstname;
		$this->lastname = $lastname;
		$this->password = $password;
		$this->role = $role;
		$this->registration = $registration;
		$this->hash = $hash;
		$this->school = $school;
		$this->school_addr = $school_addr;
		$this->street = $street;
		$this->city = $city;
		$this->newsletter = $newsletter;
		$this->activate = $activate;
	}

	public function getId() {
		return $this->id;
	}

	public function getEmail() {
		return $this->email;
	}

	public function getFirstname() {
		return $this->firstname;
	}

	public function getLastname() {
		return $this->lastname;
	}

	public function getPassword() {
		return $this->password;
	}

	public function getRole() {
		return $this->role;
	}

	public function getRegistration() {
		return $this->registration;
	}

	public function getHash() {
		return $this->hash;
	}

	public function getSchool() {
		return $this->school;
	}

	public function getSchoolAddress() {
		return $this->school_addr;
	}

	public function getStreet() {
		return $this->street;
	}

	public function getCity() {
		return $this->city;
	}

	public function getActivate() {
		return $this->activate;
	}

	public static function create($email, $firstname, $lastname, $role, $password, $hash, $school, $school_addr, $street, $city, $newsletter = 0)
	{
		global $conf;

        try {
            $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $isUnique = false;

            if (User::findByEmail($email)->getEmail() == null) {
            	$isUnique = true;
            }

            if ($isUnique) {
            	$values = "(EMAIL, FIRSTNAME, LASTNAME, ROLE_ID, PASSWORD, ACTIVATION_HASH, SCHOOL, SCHOOL_ADDR, STREET, CITY, NEWSLETTER)";
		        $query = "INSERT INTO USERS".$values." VALUES ("
					."'".$email."',"
					."'".$firstname."',"
					."'".$lastname."',"
					."'".$role."',"
					."'".hash("sha256", $password)."',"
					."'".$hash."',"
					."'".$school."',"
					."'".$school_addr."',"
					."'".$street."',"
					."'".$city."',"
					."'".$newsletter."')";

				$connection->beginTransaction();
				$connection->exec($query);
				$connection->commit();
				$connection = null;
            }
        }
        catch(PDOException $e) {
            $connection->rollback();
	    	echo "Error: " . $e->getMessage();
        }
	}

	public static function all()
	{
		global $conf;

		try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("SELECT * FROM USERS");
		    $stmt->execute();

		    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

		    return $stmt->fetchAll();
		}
		catch(PDOException $e) {
		    echo "Error: " . $e->getMessage();
		}
	}

	public static function find($id)
	{
		global $conf;

		try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("SELECT * FROM USERS WHERE ID = ". $id );
		    $stmt->execute();

		    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
		    $data = $stmt->fetch();

		    $user = new User(
		    	$data['ID'],
		    	$data['EMAIL'],
		    	$data['FIRSTNAME'],
		    	$data['LASTNAME'],
		    	$data['PASSWORD'],
		    	$data['ROLE_ID'],
		    	$data['REGISTRATION'],
		    	$data['ACTIVATION_HASH'],
					$data['SCHOOL'],
					$data['SCHOOL_ADDR'],
					$data['STREET'],
					$data['CITY']
		    );

		    return $user;
		}
		catch(PDOException $e) {
		    echo "Error: " . $e->getMessage();
		}
	}

	public static function findByFirstname($firstname)
	{
		global $conf;

		try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("SELECT * FROM USERS WHERE FIRSTNAME ='".$firstname."'");
		    $stmt->execute();

		    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
		    $data = $stmt->fetch();

		    $user = new User(
		    	$data['ID'],
		    	$data['EMAIL'],
		    	$data['FIRSTNAME'],
		    	$data['LASTNAME'],
		    	$data['PASSWORD'],
		    	$data['ROLE_ID'],
		    	$data['REGISTRATION'],
		    	$data['ACTIVATION_HASH'],
					$data['SCHOOL'],
					$data['SCHOOL_ADDR'],
					$data['STREET'],
					$data['CITY']
		    );

		    return $user;
		}
		catch(PDOException $e) {
		    echo "Error: " . $e->getMessage();
		}
	}

	public static function findByEmail($email)
	{
		global $conf;

		try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("SELECT * FROM USERS WHERE EMAIL ='".$email."'");
		    $stmt->execute();

		    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
		    $data = $stmt->fetch();

		    $user = new User(
		    	$data['ID'],
		    	$data['EMAIL'],
		    	$data['FIRSTNAME'],
		    	$data['LASTNAME'],
		    	$data['PASSWORD'],
		    	$data['ROLE_ID'],
		    	$data['REGISTRATION'],
		    	$data['ACTIVATION_HASH'],
				$data['SCHOOL'],
				$data['SCHOOL_ADDR'],
				$data['STREET'],
				$data['CITY']
		    );

		    return $user;
		}
		catch(PDOException $e) {
		    echo "Error: " . $e->getMessage();
		}
	}

	public static function isAdmin($email)
	{
		global $conf;

		try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("SELECT * FROM USERS WHERE EMAIL ='".$email."'");
		    $stmt->execute();

		    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
		    $data = $stmt->fetch();

		    if ($data['ROLE_ID'] == 1) {
		    	return 1;
		    }
		    else {
		    	return 0;
		    }
		}
		catch(PDOException $e) {
		    echo "Error: " . $e->getMessage();
		}
	}

	public static function deleteById($id)
	{
		global $conf;

		try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("DELETE * FROM USERS WHERE ID =".$id);
		    $stmt->execute();
		}
		catch(PDOException $e) {
		    echo "Error: " . $e->getMessage();
		}
	}

	public static function deleteByEmail($email)
	{
		global $conf;

		try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("DELETE * FROM USERS WHERE EMAIL =".$email);
		    $stmt->execute();
		}
		catch(PDOException $e) {
		    echo "Error: " . $e->getMessage();
		}
	}

	public function delete()
	{
		global $conf;

		try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("DELETE FROM USERS WHERE id = ". $this->id);
		    $stmt->execute();
		}
		catch(PDOException $e) {
		    echo "Error: " . $e->getMessage();
		}
	}

	public static function activate($email)
	{
		global $conf;

        try {
            $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	        $query = "UPDATE USERS SET"
				.' ACTIVATE=1'
				.' WHERE EMAIL='."'".$email."'";

			$connection->beginTransaction();
			$connection->exec($query);
			$connection->commit();
			$connection = null;
        }
        catch(PDOException $e) {
            $connection->rollback();
	    	echo "Error: " . $e->getMessage();
        }
	}

	public static function isActivated($email)
	{
		global $conf;

		try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("SELECT * FROM USERS WHERE EMAIL ='".$email."'");
		    $stmt->execute();

		    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
		    $data = $stmt->fetch();

		    if ($data['ACTIVATE'] == 1) {
		    	return 1;
		    }
		    else {
		    	return 0;
		    }
		}
		catch(PDOException $e) {
		    echo "Error: " . $e->getMessage();
		}
	}

	public static function isSignedForNewsletter($email) {
        global $conf;

        try {
            $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $connection->prepare("SELECT * FROM USERS WHERE EMAIL ='".$email."'");
            $stmt->execute();

            $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $data = $stmt->fetch();

            if ($data['NEWSLETTER'] == TRUE) {
                return TRUE;
            }
            else {
                return FALSE;
            }
        }
        catch(PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    /**
     * Sign or Unsign user from Newsletter.
     */
    public static function changeNewsletter($email, $status) {
        global $conf;

        try {
            $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            //$stmt = $connection->prepare("SELECT * FROM USERS WHERE EMAIL ='".$email."'");
            $sql = "UPDATE USERS SET newsletter=:newsletter WHERE email=:email";
            $stmt= $connection->prepare($sql);
            $stmt->execute([
                'newsletter' => $status,
                'email' => $email
            ]);

            //$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
            //$data = $stmt->fetch();
        }
        catch(PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }
}

?>
