<?php
$conf = include($_SERVER['DOCUMENT_ROOT'].'/config.php');

class Team
{
	private static $connection;
	private $id;
	private $teamNumber;
	private $user;
	private $route;


	function __construct($id,$teamNumber,$user,$route)
	{
		$this->id = $id;
		$this->teamNumber = $teamNumber;
		$this->user = $user;
		$this->route = $route;
	}

	public function getId(){
		return $this->id;
	}
	public function getTeamNumber(){
		return $this->teamNumber;
	}
	public function getUser(){
		return $this->user;
	}
	public function getRoute(){
		return $this->route;
	}

	public function create($teamNumber,$user,$route)
	{
		global $conf;

        try {
            $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $values = "(TEAMNUMBER, USER_ID, ROUTE_ID)";
	        $query = "INSERT INTO TEAMS".$values." VALUES ("
	        	."'".(int)$teamNumber."',"
	        	."'".(int)$user."',"
				."'".(int)$route."')";

			$connection->beginTransaction();
			$connection->exec($query);
			$connection->commit();
			$connection = null;
        }
        catch(PDOException $e) {
            $connection->rollback();
	    	echo "Error create: " . $e->getMessage();
        }
	}
	public function allTeams()
	{
		global $conf;

		try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("SELECT TEAMNUMBER, COUNT(*) FROM TEAMS GROUP BY TEAMNUMBER");
		    $stmt->execute();

		    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

		    return $stmt->fetchAll();

		}
		catch(PDOException $e) {
		    echo "Error allTeams:" . $e->getMessage();
		}

	}
	public function findAllInTeam($teamNumber)
	{
		global $conf;

		try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("SELECT a.id,b.FIRSTNAME,b.LASTNAME,b.EMAIL,a.TEAMNUMBER FROM TEAMS a join USERS b ON a.USER_ID = b.ID WHERE TEAMNUMBER = ".$teamNumber."");

		    $stmt->execute();

		    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

		    return $stmt->fetchAll();

		}
		catch(PDOException $e) {
		    echo "Error findAllInTeam: " . $e->getMessage();
		}
	}
	public function removeById($id)
	{
		global $conf;

		try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("DELETE FROM TEAMS WHERE ID = ".$id."");
		    $stmt->execute();
		}
		catch(PDOException $e) {
		    echo "Error removeById: " . $e->getMessage();
		}
	}

	public function insertByEmail($teamNumber,$email)
	{
		global $conf;
		if(intval(Team::inTeam($teamNumber)) < 6){
		try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("INSERT INTO TEAMS (TEAMNUMBER, USER_ID) SELECT ".$teamNumber.",ID FROM USERS WHERE EMAIL = '".$email."' AND NOT EXISTS(SELECT * FROM TEAMS a join USERS b on a.USER_ID = b.ID WHERE b.EMAIL = '".$email."')");

		    $stmt->execute();
		}
		catch(PDOException $e) {
		    echo "Error removeById: " . $e->getMessage();
		}
		}
	}

	public function findAllTeamNumbers()
	{
			global $conf;

			try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("SELECT TEAMNUMBER FROM TEAMS GROUP BY TEAMNUMBER");

		    $stmt->execute();
		    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

		    return $stmt->fetchAll();
		}
		catch(PDOException $e) {
		    echo "Error removeById: " . $e->getMessage();
		}
	}
	public function inTeam($teamNumber)
	{
		global $conf;

			try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("SELECT TEAMNUMBER,COUNT(*) FROM TEAMS WHERE TEAMNUMBER = ".$teamNumber." GROUP BY TEAMNUMBER");

		    $stmt->execute();
		    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
		    $result = $stmt->fetchAll();
		    foreach ($result as $pocet) {
		    	$mnozstvo = $pocet['COUNT(*)'];
		    }
		    return $mnozstvo;
			}
			catch(PDOException $e) {
		    echo "Error removeById: " . $e->getMessage();
			}
	}
	public function removeTeam($teamNumber)
	{
		global $conf;

			try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("DELETE FROM TEAMS WHERE TEAMNUMBER = ".$teamNumber."");
		    $stmt->execute();
			}
			catch(PDOException $e) {
		    echo "Error removeTeam: " . $e->getMessage();
			}
	}
	public function usersToRoute($routeID)
	{
		global $conf;

			try {
		    $connection = new PDO("mysql:host=".$conf['host'].";dbname=".$conf['db_name']."", $conf['username'], $conf['password']);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $stmt = $connection->prepare("SELECT USER_ID FROM TEAMS where TEAMNUMBER = (SELECT a.TEAMNUMBER FROM TEAMS a JOIN USERS b ON a.USER_ID = b.ID JOIN ROUTES c on b.ID = c.USER_ID WHERE c.ID = ".$routeID." and c.ROUTE_MODE = 'relay')");

		    $stmt->execute();
		    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
		    $result = $stmt->fetchAll();
		    $ludia = array();
		    foreach ($result as $pocet) {
		    	array_push($ludia,$pocet['USER_ID']);
		    }
		    return $ludia;
			}
			catch(PDOException $e) {
		    echo "Error removeTeam: " . $e->getMessage();
			}

	}
}
?>
