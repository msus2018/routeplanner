<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

if ( ! isset( $conf ) ) {
    $conf = require_once( $_SERVER['DOCUMENT_ROOT'] . '/config.php' );
}

require_once( $_SERVER['DOCUMENT_ROOT'] . '/Libraries/PHPMailer/Exception.php' );
require_once( $_SERVER['DOCUMENT_ROOT'] . '/Libraries/PHPMailer/PHPMailer.php' );
require_once( $_SERVER['DOCUMENT_ROOT'] . '/Libraries/PHPMailer/SMTP.php' );


class Actuality {
    private static $connection;
    private $id;
    private $headline;
    private $content;

    public function __construct( $headline, $content ) {
        $this->headline = $headline;
        $this->content  = $content;
    }

    /**
     * Get all actualities from DB.
     */
    public static function all() {
        global $conf;

        try {
            $connection = new PDO( "mysql:host=" . $conf['host'] . ";dbname=" . $conf['db_name'] . "", $conf['username'], $conf['password'] );
            $connection->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            $stmt = $connection->prepare( "SELECT * FROM ACTUALITIES" );
            $stmt->execute();

            $result = $stmt->setFetchMode( PDO::FETCH_ASSOC );

            return $stmt->fetchAll();
        } catch( PDOException $e ) {
            echo "Error: " . $e->getMessage();
        }
    }

    /**
     * Save actuality into DB.
     * Send newsletter to signed users.
     */
    public function save() {
        /**
         * Send email to everybody who is signed up.
         */
        global $conf;
        try {
            $connection = new PDO( "mysql:host=" . $conf['host'] . ";dbname=" . $conf['db_name'] . "", $conf['username'], $conf['password'] );
            $connection->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

            $values = "(HEADLINE, CONTENT)";
            $query  = "INSERT INTO ACTUALITIES" . $values . " VALUES ("
                      . "'" . $this->headline . "',"
                      . "'" . $this->content . "')";

            $connection->beginTransaction();
            $connection->exec( $query );
            $connection->commit();
            $connection = NULL;

            $this->sendNewsletter($this->headline, $this->content);
        } catch( PDOException $e ) {
            $connection->rollback();
            echo "Error: " . $e->getMessage();
        }
    }

    /**
     * Send newsletter.
     */
    private function sendNewsletter($headline, $content) {
        global $conf;

        try {
            $connection = new PDO( "mysql:host=" . $conf['host'] . ";dbname=" . $conf['db_name'] . "", $conf['username'], $conf['password'] );
            $connection->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            $stmt = $connection->prepare( "SELECT * FROM USERS WHERE NEWSLETTER != 0" );
            $stmt->execute();
            $stmt->setFetchMode( PDO::FETCH_ASSOC );

            $users = $stmt->fetchAll();
            if (sizeof($users) == 0) {
                return;
            }

            $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
            try {
                //Server settings
                $mail->SMTPDebug = 2;                                 // Enable verbose debug output
                $mail->isSMTP();                                      // Set mailer to use SMTP
								$mail->Host = 'smtp1.example.com;smtp2.example.com';  // Specify main and backup SMTP servers
								$mail->SMTPAuth = false;                               // Enable SMTP authentication
								$mail->Username = 'user@example.com';                 // SMTP username
								$mail->Password = 'secret';                           // SMTP password
								$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
								$mail->Port = 587;                                    // TCP port to connect to
                $mail->IsHTML(TRUE);

                //Recipients
                $mail->setFrom('xkurilla@stuba.sk', 'Juraj');
                foreach ( $users as $user) {
                    $mail->addAddress($user['EMAIL']);
                }

                //Content
                $mail->isHTML(true);                                  // Set email format to HTML
                $mail->Subject = $headline;
                $mail->Body    = $content;

                $mail->send();
                exit;
            } catch (Exception $e) {
                echo '<pre>';
                print_r( $mail->ErrorInfo );
                echo '</pre>';
                exit;
                echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
            }

            /*foreach ()*/
        } catch( PDOException $e ) {
            echo "Error: " . $e->getMessage();
        }
    }

    /**
     * @return mixed
     */
    public static function getConnection() {
        return self::$connection;
    }

    /**
     * @param mixed $connection
     */
    public static function setConnection( $connection ) {
        self::$connection = $connection;
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId( $id ) {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getHeadline() {
        return $this->headline;
    }

    /**
     * @param mixed $headline
     */
    public function setHeadline( $headline ) {
        $this->headline = $headline;
    }

    /**
     * @return mixed
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent( $content ) {
        $this->content = $content;
    }

}
