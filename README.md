## Intro
Takže vytvoril som repozitár na bitbuckete. Tu budeme pridávať a sťahovať naše nové verzie. V našej aplikácií je zataľ možné sa zaregistrovať a prihlásiť sa (to aby sme mali zatiaľ aspoň niečo :) ).

## Štruktúra projektu

Vytvoril som základnú štruktúru projektu, ktorá pozostáva z nasledujúcich súborov/adresárov:

1. Adresár **View** uchováva všetky .php stránky, ktoré niečo zobrazujú. Všetky majú tvar **file.view.php**, napr. signin.view.php
2. Podadresár **Blade** uchováva všetky "šablóny", ktoré majú formu **file.blade.php**, napr. footer.blade.php. Obsahujú znovupoužívané časti kódu, kao napr. navbar, footer,...
3. Adresár **BusinessLogic** obsahuje všetku logiku nášho zadania. Je to akoby naše API. Spracúvava a vracia dáta. Má formát **file.php**, napr. signin.php
4. Adresár **Model** obsahuje súbory, ktoré cez PDO (objektový prístup k DB), pristupujú k DB. Každý Model predstavuje jednu tabuľku. Napr. **User.php**.
5. Súbor **index.php** je naším hlavným súborom.
6. Súbor **config.php** obsahuje dáta potrebné pre načítanie databázy. K tomuto súboru sa ešte vrátim.
7. Súbor **RoutePlanner.sql** obsahuje sql_dump databázy. Importniteho a máte presne tú istú databázu ako zvyšok týmu.
8. V súbor **.gitignore** sú súbory, ktoré GIT ignoruje, nie sú trackované (nesledujú sa na nich zmeny)
9. Adresár **Libraries** uchováva PHP libky, napr. phpmailer.

## Config.php
Súbor config.php sa nenachádza v repozitáry pretože každý z nás bude mať iné údaje. Má ale predpísaný tvar, ktorý je potrebný inka s ním aplikácia nebude vedieť pracovať.
Predpokladá sa, že appku máte v priečinku routeplanner/

	return array(
		'host' => 'hostname',
		'username' => 'username',
		'password' => 'password',
		'db_name' => 'RoutePlanning',
		'base_url' => 'http://localhost/routeplanner/'
	);

**db_name** nechajte tak ako je. V repozitáry sa totižto nachádza aj sql_dump, ktorý ked si importnene do databázy(neskôr DB), tak budeme mať aktuálnu DB. Rovnako ak niekto z vás bude pracovať a meniť DB, zmente aj sql_dump.

## Rules
Bolo by super ak by sme všetkci používali rovnaké prístupy. Čiže dodržovať štruktúru aká je teraz. Zobrazovacie stránky ukladať do **View**, súbory zodpovedné za databázu ukladať do **Model** a logiku programu (dá sa povedať aj "naše API") ukladať do **BusinessLogic**.

Pre prístup do DB používať PDO, čiže objektový prístup, ktorý je mnohonásobne lepší, najmä kvôli udržateľnosti a znovupoužiteľnosti. Čo sa týka tried, skúste si pozriež ako vyzerá napr. trieda **User.php**. Bolo by fajn zachovať štruktúru triedy. Našiel som to na nete a je to prehľadné.

Tiež by bolo super dodržovať tvar súborov. Niekedy sa totižto súbory volajú rovnako, napr. **signin.view.php** a **signin.php**. Koncovka **.view.php** jednoznačne určuje, o aký typ súboru sa jedná. Ide o súbor, ktorý zobrazuje prihlasovaciu stránku. Rovnako to platí aj pre **.blade.php**

Ak budete mať nejaké súbory, ktoré sa často menia, alebo u nás sa aj tak prepíšu (ako napr. config.php), vložte ich to súboru **.gitinore**. Nech nie sú zbytočne v repozotáry a nespôsobujú chaos typu: Čo je to za súbor? Ja ho mám iný?!

## Git
Neviem nakoľko poznáte Git. Ale bolo by super sa dopredu dohodnúť na našom work flow. Pretože ńajhoršie na Gite je mergovanie vetiev. Čiže ak ja som začal robiť na starej verzíi a Maťo ( :) ) ju zatiaľ upravil a pushol, tak keď ju budem chcieť pushnúť ja, nastane kolízia. Tie bude treba riešiť. A to je nepríjemné.

Rovnako by bolo super, ak by ste komentovali svoj kód, a tiež svoje commity. Čiže ak niečo nové vložíte do repozitára, okomentujte to priamo na bitbucket. Ostatní si potom vedia pozrieť na čom ste robili.

Užitočné linky:

https://git-scm.com/book/cs/v1/%C3%9Avod
https://www.sbf5.com/~cduan/technical/git/git-1.shtml

## Clone a repository
Repozitár si naklonujete príkazom:

git clone https://xstaryj@bitbucket.org/msus2018/routeplanner.git
